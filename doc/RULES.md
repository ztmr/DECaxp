# Project Notes:
    The project structure is as follows:
    1. Documentation is located in the `doc/` directory and informational documentation in the `exrtra/` directory.
    2. Build (CMakeLists.txt) are located in the each directory containing compileable code.
    3. Source code is in the following sub-directories found in `src/`:
        a. com-util/ contains a common set of utilities (used across multiple components).
        b. cache - contains the Icache and Dcache code
        c. c-box - contains the code that interfaces between the rest of the CPU, the Bcache, and System
        d. e-box - contains the integer execution code
        e. f-box - contains the floating-point execution code
        f. i-box - contains the instruction processing code, accessing the iCache
        g. m-box - contains the dCache processing code
        h. c-chip - contains the Controller Chip (Cchip) code and controls the Dchip, Pchip, Memory access, and CPUs.
        i. d-chip - contains the Data Controller Chip code and moves data from memory to/from the Pchip and to/from CPUs,
           at the direction of the Cchip
        j. p-chip - contains the PCI Control Chip code and contains moves data to/from PCI devices and data from/to Cchip
           to/from Dchip.
        k. console - contains the TELNET server code where the console is emulated.
        l. ethernet - contains the code that allows for data transmission through the host system ethernet device.
        m. toyclock - contains the code that emulates the Time-Of-Year (TOY) clock.
        n. vhd - contains the code that implements Virtual Hard Disks, using industry standard layouts.
        o. sys-inter - contains the code that is used to interface the CPUs and the Cchip and Dchip.
    4. Source code in the `src/` directory are:
        a. DECaxp
        b. DECaxp Generate SROM
    5. Test code is found in the `test/` directory..
    4. Data files are located in the `dat/` folders.
    5. Binary files from compiling are placed in the `bin/` folder (note: GIT ignores this folder's contents)

    When building the project:
    1. Use the `build.sh` script in the top-level directory to build the programs into the `bin/exe/` directory.

# Coding Rules:
    The code is written entirely in the C language.  The ANSI C compiler option
    should be utilized.  When in doubt, look at existing code for an example of
    how the code should be formatted.

    Coding Style:
        Basics:
            blank lines:    Blank lines should not have any whitespace
                            characters.
            tab length:     Each tab is 4 space characters long and comprised
                            of spaces only (no tabs).
            white space:    There will be no whitespace characters at the end
                            of a line.
            line length:    A line of code should not exceed 80 characters
                            long.  If at all possible, and code clarity is not
                            lost, a line of code which exceeds 80 characters
                            should be broken up into multiple lines.
        Comments:
            module comment: Every module/header file will begin with a
                            copyright comment, also containing a module/header
                            description and revision history.  The first
                            revision history entry will be V01.000 and say
                            "Initially written.".
            line comments:  Only '/* */' can be used for Line comments.  Line
                            comments should either be on their own line or at
                            the and of a line containing code.  If a line
                            comment is on its own line, then the start of the
                            comment is in line with the code underneath.  Also,
                            these line comments will be proceeded by a blank
                            line.
            multiline comments:
                            There will be a blank line before a multiline
                            comment. The comment, itself, will start with '/*',
                            where the '/' is inline with the underlying code.
                            The comment start will be on it's own line.
                            The comment will end with '*/', where the '*' will
                            be inline with the '*' on on the comment start and
                            be ib its own line.
                            Lines in between the start and end comment will
                            a '*', which is inline with the '*' on both the
                            comment start and end.  If an in-between comment
                            does not have any text following it, then the white
                            space rule above shall be followed.  Otherwise, one
                            or more sace/tab characters will follow the '*',
                            prior to the text of the comment.
                            There will be no blank line following a multi-line
                            comment, unless the comment stands along and is not
                            specifically comment in the subsequent code.
                            A variation on the multiline comment mat be used,
                            where the opening comment '/*' be be followed by
                            multiple '*', and inbetweem comments can contain
                            space characters prior to a final '*', and the
                            comment end can be preceeded by mutliple '*', all
                            to form a box.
            function comments:
                            All functions will be proceeded by a multiline
                            comment.  The function comment will have the
                            following format:
``` {.c}
                                /*
                                 * functionName
                                 *  Function description.
                                 *
                                 * Input Parameters:
                                 *  param1:
                                 *      Input Param1 description.
                                 *  paramn:
                                 *      Input Paramn description.
                                 * Output Parameters:
                                 *  paramx:
                                 *      Output Paramx description.
                                 *  paramy:
                                 *      Output Paramy description.
                                 *
                                 * Return Value:
                                 *  None or a list of potential return values
                                 *  and what they mean.
                                 */
```
                            A parameter is this both an input and output
                            parameter may be listed in both areas.
        Files:
            header file:    After the copyright comment. the first two lines
                            should be the following:
``` {.c}
                                #ifndef _HEADER_FILE_NAME_DEFS_
                                #define _HEADER_FILE_NAME_DEFS_
```
                            The last line should be the following (note a tab
                            character between #endif and '/*'):
``` {.c}
                                #endif    /* _HEADER_FILE_NAME_DEFS_ */
```
                            There shall be at least one header file for each
                            module.  A module file of the name foobar.c, shall
                            have a header file of foobar.h.  This header file
                            will include all the files needed by the module.
                            The module will include this header file.
                            For external entry points, either the module header
                            file or a separate header file (with a file name
                            sufficient to determine what the header file
                            contains) will be utilized.
                            A header file will contain no code that is compiled
                            within the header file.
            includes:       All includes are listed at the top of the header
                            and module file.  Conditional includes may be done
                            here as well.
                            A header file should contain all the includes
                            required for it to be successfully compiled (even
                            by itself).
            module file:    A module file will have a similar format to a
                            header file, except it will not contain the
                            '#ifndef ... #endif' lines.
                            A module file can contain everything a header file
                            includes, plus code that will be compiled into an
                            executeable.
                            All global variables (module or beyond), will be
                            declared at the top of the module (after any
                            includes).
                            All functions/routines/procedures will be preceeded
                            by a function comment (see above).
        Code:
            All code will have the following format (note locations of '{' and
            white space):
``` {.c}
            /*
             * functionName
             *  Function description.
             *
             * Input Parameters:
             *  param1:
             *      Input Param1 description.
             *
             * Output Parameters:
             *  None.
             *
             * Return Value:
             *  1:  Item not found.
             *  2:  Close enough item found.
             *  3:  Item found.
             */
            int funct(int param)
            {
                int retVal = 1;
                int ii;

                /*
                 * Loop until param and retVal are not equal.
                 */
                while (param == retVal)
                {

                    /*
                     * Loop through param until we find what we came for.
                     */
                    for (ii = 0; ii < param; ii++)
                    {
                        if (param)
                        {
                            retVal = 2;        // Preliminary value.
                        }
                        else
                        {
                            retVal = 3;        // What we came for.
                            break;            // get out of for loop.
                        }
                    }

                    /*
                     * Retest while condition to see if we are done.
                     */
                    continue;
                }

                /*
                 * Return what we found back to the caller.
                 */
                return(RetVal);
            }
```
