#  Introduction

The DECaxp application requires a number of additional packages to be installed
before being able to successfully build it.

**Update**: I started looking at this code again.  It may take me some time to
get familiar with it again.  That being said, I fixed most of the current set
of issues.  All test programs run successfully, including AXP_Test_Telnet.
There is a potential segmentation fault when DECaxp tries to call the System
code.  This is a known issue and the System code is going to be completely
rewritten (I've spent much of my off time reading the HRMs very carefully).

##Compiler Change

The `CMakeLists.txt` file has been updated to compile with clang 9.  Please
make sure to have that compiler ready.  The reason for this is the clang
compiles much faster and it is better at finding issues that should be fixed.

**Note**: These build instructions were generated when building on
Ubuntu-Budgie 19.10.  The items listed here should be consistent with versions
prior and following the indicated Ubuntu release.  Building on other Linux
releases or Cygwin may be require different packages.  Your mileage may vary.

## Prerequisites

The following packages are required to be able to build DECaxp successfully.

 # cmake
 # libpcap0.8-dev
 # libpcap-dev
 # libxml++2.6-dev
 # clang 9.0
 
```{.bash}
sudo apt install cmake libpcap0.8-dev libpcap-dev libxml++2.6-dev
```

**Note**: The prerequisite packages may change as development continues and
prior to this file getting updated.

## Running the build
 
The `build.sh` file contains the steps required to build DECaxp.  This script
contains help text to allow you to instruct it how and what to build.
 
```{.bash}
$ ./build.sh -h

Usage: build.sh [options]

Options:
 -h, --help     display this help
 -d, --debug    set CMAKE_BUILD_TYPE=Debug (default = Release)
 -c, --clean    run make clean followed by make
 -r, --reset    delete the build directory and run make clean followed by make

If the build directory exists, and --reset is not specified then only make will be run
```

The generated executables will be put in the `<project-directory>/build/Executables/`
directory.

A successful build should generate the following output:

```{.bash}
$  ./build.sh -r
-- The C compiler identification is GNU 9.2.1
-- The CXX compiler identification is GNU 9.2.1
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/belanger/projects/DECaxp/build
Scanning dependencies of target CommonUtilities
[  1%] Building C object CommonUtilities/CMakeFiles/CommonUtilities.dir/AXP_Blocks.c.o
[  2%] Building C object CommonUtilities/CMakeFiles/CommonUtilities.dir/AXP_Configure.c.o
[  3%] Building C object CommonUtilities/CMakeFiles/CommonUtilities.dir/AXP_Dumps.c.o
[  4%] Building C object CommonUtilities/CMakeFiles/CommonUtilities.dir/AXP_Exceptions.c.o
[  5%] Building C object CommonUtilities/CMakeFiles/CommonUtilities.dir/AXP_Execute_Box.c.o
[  6%] Building C object CommonUtilities/CMakeFiles/CommonUtilities.dir/AXP_NameValuePair_Read.c.o
[  7%] Building C object CommonUtilities/CMakeFiles/CommonUtilities.dir/AXP_StateMachine.c.o
[  8%] Building C object CommonUtilities/CMakeFiles/CommonUtilities.dir/AXP_Trace.c.o
[  9%] Building C object CommonUtilities/CMakeFiles/CommonUtilities.dir/AXP_Utility.c.o
[ 10%] Linking C static library libCommonUtilities.a
[ 10%] Built target CommonUtilities
Scanning dependencies of target CPU
[ 11%] Building C object CPU/CMakeFiles/CPU.dir/AXP_21264_CPU.c.o
[ 12%] Linking C static library libCPU.a
[ 12%] Built target CPU
Scanning dependencies of target Caches
[ 13%] Building C object CPU/Caches/CMakeFiles/Caches.dir/AXP_21264_Cache.c.o
[ 14%] Building C object CPU/Caches/CMakeFiles/Caches.dir/AXP_21264_BCache.c.o
[ 15%] Linking C static library libCaches.a
[ 15%] Built target Caches
Scanning dependencies of target Cbox
[ 16%] Building C object CPU/Cbox/CMakeFiles/Cbox.dir/AXP_21264_Cbox_IOWB.c.o
[ 17%] Building C object CPU/Cbox/CMakeFiles/Cbox.dir/AXP_21264_Cbox_MAF.c.o
[ 18%] Building C object CPU/Cbox/CMakeFiles/Cbox.dir/AXP_21264_Cbox_PQ.c.o
[ 19%] Building C object CPU/Cbox/CMakeFiles/Cbox.dir/AXP_21264_Cbox_VDB.c.o
[ 20%] Building C object CPU/Cbox/CMakeFiles/Cbox.dir/AXP_21264_Cbox.c.o
[ 21%] Building C object CPU/Cbox/CMakeFiles/Cbox.dir/SystemInterface/AXP_21264_to_System.c.o
[ 22%] Linking C static library libCbox.a
[ 22%] Built target Cbox
Scanning dependencies of target Ebox
[ 23%] Building C object CPU/Ebox/CMakeFiles/Ebox.dir/AXP_21264_Ebox_Byte.c.o
[ 24%] Building C object CPU/Ebox/CMakeFiles/Ebox.dir/AXP_21264_Ebox_Control.c.o
[ 25%] Building C object CPU/Ebox/CMakeFiles/Ebox.dir/AXP_21264_Ebox_IntegerMath.c.o
[ 26%] Building C object CPU/Ebox/CMakeFiles/Ebox.dir/AXP_21264_Ebox_LoadStore.c.o
[ 27%] Building C object CPU/Ebox/CMakeFiles/Ebox.dir/AXP_21264_Ebox_LogicalShift.c.o
[ 28%] Building C object CPU/Ebox/CMakeFiles/Ebox.dir/AXP_21264_Ebox_Misc.c.o
[ 29%] Building C object CPU/Ebox/CMakeFiles/Ebox.dir/AXP_21264_Ebox_Multimedia.c.o
[ 30%] Building C object CPU/Ebox/CMakeFiles/Ebox.dir/AXP_21264_Ebox_PALFunctions.c.o
[ 31%] Building C object CPU/Ebox/CMakeFiles/Ebox.dir/AXP_21264_Ebox_VAX.c.o
[ 32%] Building C object CPU/Ebox/CMakeFiles/Ebox.dir/AXP_21264_Ebox.c.o
[ 33%] Linking C static library libEbox.a
[ 33%] Built target Ebox
Scanning dependencies of target Fbox
[ 34%] Building C object CPU/Fbox/CMakeFiles/Fbox.dir/AXP_21264_Fbox_Control.c.o
[ 35%] Building C object CPU/Fbox/CMakeFiles/Fbox.dir/AXP_21264_Fbox_FPFunctions.c.o
[ 36%] Building C object CPU/Fbox/CMakeFiles/Fbox.dir/AXP_21264_Fbox_LoadStore.c.o
[ 37%] Building C object CPU/Fbox/CMakeFiles/Fbox.dir/AXP_21264_Fbox_OperateIEEE.c.o
[ 38%] Building C object CPU/Fbox/CMakeFiles/Fbox.dir/AXP_21264_Fbox_OperateMisc.c.o
[ 39%] Building C object CPU/Fbox/CMakeFiles/Fbox.dir/AXP_21264_Fbox_OperateVAX.c.o
[ 40%] Building C object CPU/Fbox/CMakeFiles/Fbox.dir/AXP_21264_Fbox.c.o
[ 41%] Linking C static library libFbox.a
[ 41%] Built target Fbox
Scanning dependencies of target Ibox
[ 42%] Building C object CPU/Ibox/CMakeFiles/Ibox.dir/AXP_21264_Ibox_Initialize.c.o
[ 43%] Building C object CPU/Ibox/CMakeFiles/Ibox.dir/AXP_21264_Ibox_InstructionDecoding.c.o
[ 44%] Building C object CPU/Ibox/CMakeFiles/Ibox.dir/AXP_21264_Ibox_InstructionInfo.c.o
[ 45%] Building C object CPU/Ibox/CMakeFiles/Ibox.dir/AXP_21264_Ibox_PCHandling.c.o
[ 46%] Building C object CPU/Ibox/CMakeFiles/Ibox.dir/AXP_21264_Ibox_Prediction.c.o
[ 47%] Building C object CPU/Ibox/CMakeFiles/Ibox.dir/AXP_21264_Ibox.c.o
[ 48%] Linking C static library libIbox.a
[ 48%] Built target Ibox
Scanning dependencies of target Mbox
[ 49%] Building C object CPU/Mbox/CMakeFiles/Mbox.dir/AXP_21264_Mbox.c.o
[ 50%] Linking C static library libMbox.a
[ 50%] Built target Mbox
Scanning dependencies of target Motherboard
[ 51%] Building C object Motherboard/CMakeFiles/Motherboard.dir/AXP_21274_AddressMapping.c.o
[ 52%] Building C object Motherboard/CMakeFiles/Motherboard.dir/AXP_21274_System.c.o
[ 53%] Linking C static library libMotherboard.a
[ 53%] Built target Motherboard
Scanning dependencies of target Cchip
[ 54%] Building C object Motherboard/Cchip/CMakeFiles/Cchip.dir/AXP_21274_Cchip.c.o
[ 55%] Building C object Motherboard/Cchip/CMakeFiles/Cchip.dir/CPUInterface/AXP_21274_to_CPU.c.o
[ 56%] Linking C static library libCchip.a
[ 56%] Built target Cchip
Scanning dependencies of target Dchip
[ 57%] Building C object Motherboard/Dchip/CMakeFiles/Dchip.dir/AXP_21274_Dchip.c.o
[ 58%] Linking C static library libDchip.a
[ 58%] Built target Dchip
Scanning dependencies of target Pchip
[ 59%] Building C object Motherboard/Pchip/CMakeFiles/Pchip.dir/AXP_21274_Pchip.c.o
[ 60%] Linking C static library libPchip.a
[ 60%] Built target Pchip
Scanning dependencies of target Console
[ 61%] Building C object Devices/Console/CMakeFiles/Console.dir/AXP_Telnet.c.o
[ 62%] Linking C static library libConsole.a
[ 62%] Built target Console
Scanning dependencies of target TOYClock
[ 63%] Building C object Devices/DS12887A_TOYClock/CMakeFiles/TOYClock.dir/AXP_DS12887A_TOYClock.c.o
[ 64%] Linking C static library libTOYClock.a
[ 64%] Built target TOYClock
Scanning dependencies of target Ethernet
[ 65%] Building C object Devices/Ethernet/CMakeFiles/Ethernet.dir/AXP_Ethernet.c.o
[ 66%] Linking C static library libEthernet.a
[ 66%] Built target Ethernet
Scanning dependencies of target VirtualDisks
[ 67%] Building C object Devices/VirtualDisks/CMakeFiles/VirtualDisks.dir/AXP_RAW.c.o
[ 68%] Building C object Devices/VirtualDisks/CMakeFiles/VirtualDisks.dir/AXP_SSD.c.o
[ 69%] Building C object Devices/VirtualDisks/CMakeFiles/VirtualDisks.dir/AXP_VHD_Utility.c.o
[ 70%] Building C object Devices/VirtualDisks/CMakeFiles/VirtualDisks.dir/AXP_VHD.c.o
[ 71%] Building C object Devices/VirtualDisks/CMakeFiles/VirtualDisks.dir/AXP_VHDX.c.o
[ 72%] Building C object Devices/VirtualDisks/CMakeFiles/VirtualDisks.dir/AXP_VirtualDisk.c.o
[ 73%] Linking C static library libVirtualDisks.a
[ 73%] Built target VirtualDisks
Scanning dependencies of target DECaxp_Generate_SROM
[ 74%] Building C object DECaxp/CMakeFiles/DECaxp_Generate_SROM.dir/DECaxp_Generate_SROM.c.o
[ 75%] Linking C executable ../Executables/DECaxp_Generate_SROM
[ 75%] Built target DECaxp_Generate_SROM
Scanning dependencies of target DECaxp
[ 76%] Building C object DECaxp/CMakeFiles/DECaxp.dir/DECaxp.c.o
[ 77%] Linking C executable ../Executables/DECaxp
[ 77%] Built target DECaxp
Scanning dependencies of target AXP_21264_FloatingPointTest_Test
[ 78%] Building C object Tests/CMakeFiles/AXP_21264_FloatingPointTest_Test.dir/AXP_21264_FloatingPointTest.c.o
[ 79%] Linking C executable ../Executables/AXP_21264_FloatingPointTest_Test
[ 79%] Built target AXP_21264_FloatingPointTest_Test
Scanning dependencies of target AXP_21264_IntegerLoadTest
[ 80%] Building C object Tests/CMakeFiles/AXP_21264_IntegerLoadTest.dir/AXP_21264_IntegerLoadTest.c.o
[ 81%] Linking C executable ../Executables/AXP_21264_IntegerLoadTest
[ 81%] Built target AXP_21264_IntegerLoadTest
Scanning dependencies of target AXP_21264_Dump_Test
[ 82%] Building C object Tests/CMakeFiles/AXP_21264_Dump_Test.dir/AXP_21264_Dump_Test.c.o
[ 83%] Linking C executable ../Executables/AXP_21264_Dump_Test
[ 83%] Built target AXP_21264_Dump_Test
Scanning dependencies of target AXP_21264_Prediction_Test
[ 84%] Building C object Tests/CMakeFiles/AXP_21264_Prediction_Test.dir/AXP_21264_Prediction_Test.c.o
[ 85%] Linking C executable ../Executables/AXP_21264_Prediction_Test
[ 85%] Built target AXP_21264_Prediction_Test
Scanning dependencies of target AXP_Test_Npcap
[ 86%] Building C object Tests/CMakeFiles/AXP_Test_Npcap.dir/AXP_Test_Npcap.c.o
[ 87%] Linking C executable ../Executables/AXP_Test_Npcap
[ 87%] Built target AXP_Test_Npcap
Scanning dependencies of target AXP_Test_Queues
[ 88%] Building C object Tests/CMakeFiles/AXP_Test_Queues.dir/AXP_Test_Queues.c.o
[ 89%] Linking C executable ../Executables/AXP_Test_Queues
[ 89%] Built target AXP_Test_Queues
Scanning dependencies of target AXP_Disk_Test
[ 90%] Building C object Tests/CMakeFiles/AXP_Disk_Test.dir/AXP_Disk_Test.c.o
[ 91%] Linking C executable ../Executables/AXP_Disk_Test
[ 91%] Built target AXP_Disk_Test
Scanning dependencies of target AXP_DS12887A_Test
[ 92%] Building C object Tests/CMakeFiles/AXP_DS12887A_Test.dir/AXP_DS12887A_Test.c.o
[ 93%] Linking C executable ../Executables/AXP_DS12887A_Test
[ 93%] Built target AXP_DS12887A_Test
Scanning dependencies of target AXP_21264_Icache_Test
[ 94%] Building C object Tests/CMakeFiles/AXP_21264_Icache_Test.dir/AXP_21264_Icache_Test.c.o
[ 95%] Linking C executable ../Executables/AXP_21264_Icache_Test
[ 95%] Built target AXP_21264_Icache_Test
Scanning dependencies of target AXP_Telnet_Test
[ 96%] Building C object Tests/CMakeFiles/AXP_Telnet_Test.dir/AXP_Telnet_Test.c.o
[ 97%] Linking C executable ../Executables/AXP_Telnet_Test
[ 97%] Built target AXP_Telnet_Test
Scanning dependencies of target AXP_21264_Cache_Test
[ 97%] Building C object Tests/CMakeFiles/AXP_21264_Cache_Test.dir/AXP_21264_Cache_Test.c.o
[ 98%] Linking C executable ../Executables/AXP_21264_Cache_Test
[ 98%] Built target AXP_21264_Cache_Test
Scanning dependencies of target AXP_Test_Structure_Sizes
[ 99%] Building C object Tests/CMakeFiles/AXP_Test_Structure_Sizes.dir/AXP_Test_Structure_Sizes.c.o
[100%] Linking C executable ../Executables/AXP_Test_Structure_Sizes
[100%] Built target AXP_Test_Structure_Sizes
```

**Note**: As development continues, the above output will change.  The above is the output
generated at the time of the initial writing of this document.
