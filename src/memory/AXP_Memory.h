/*
 * Copyright (C) Jonathan D. Belanger 2019-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains definitions needed to implement the physical
 *  memory characteristic for an Alpha 21264 System using either the
 *  Tsunami (21272) or Typhoon (21274) Chip Set.
 *
 * Revision History:
 *
 *  V01.000 26-Dec-2019 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _MEMORY_AXP_MEMORY_H_
#define _MEMORY_AXP_MEMORY_H_

#include "com-util/AXP_System_Common.h"

/*
 * The difference between a 21272 and 21274 chip set are as follows:
 *
 *                            21272             21274
 *                      -----------         ---------
 *  1) CPUs                     1-2               1-4
 *  2) Array Sizes          16, 32,        Same, plus
 *                         64, 128,         2, 4, 8GB
 *                      256, 512MB,
 *                              1GB
 *  3) Memory layout     Non-split,         Same, plus
 *                            Split        Twice Split
 *
 * Common functionality:
 *
 *  1) Memory is organized in cache block sizes (octaword - 64 bytes).
 *  2) Partial octaword access is possible, but only for PCI DMA writes.
 *  3) There are between 1 and 4 memory arrays, with each of these arrays being
 *     a different size.  The system code will provide the functionality to
 *     prevent holes in memory that could be caused by the differing array
 *     sizes.
 *
 * Specifics about this implementation:
 *
 * The memory is broken up into 4 arrays.  The memory arrays are NOT broken up
 * into sub-arrays.  This is not needed to enhance performance.  Each array has
 * a read/write access associated with it.  Up to 4 reads, 4 writes, or any
 * combination of reads and writes adding up to 4.  The memory arrays will NOT
 * have an associated mutex with it.  It will be up to the Cchip to coordinate
 * the reads and writes.  This is so that the implementation is as fast as
 * possible.
 *
 * Because the emulator is not limited by real estate, it is assumed that an
 * entire block can be moved in and out of memory in a single operation.  There
 * will be a single read buffer and a single write buffer per array.  When the
 * Dchip is so instructed, by the Cchip, it will either move a block into a
 * write buffer or out of a read buffer for a particular array.  The Dchip does
 * not directly access the memory arrays, but places/fetches data into one of
 * these buffers.
 *
 * The memory module has an interface for the Cchip to be able to request to
 * have memory written to or read from.  The interface can be accessed by
 * multiple threads simultaneously, but only once per memory array.  There will
 * be no code that will try to make the Cchip behave.  It is up to the Cchip to
 * get this right.
 *
 * The memory modules use read/write buffers provided by the Dchip to store a
 * block read from memory, or read a block or partial block to be written to
 * memory.  There will be one pair of buffers per memory array.  There will be
 * NO mutexes used to prevent simultaneous access to one of these buffers.
 * Again, the Cchip is responsible for managing the access to these buffers so
 * that there will never be multiple attempts to access these buffers at the
 * same time.
 *
 * Memory can be any size between 1MB and 4GB (for 21272)/32GB (for 21274).
 * Arrays will be sized according to the four arrays being the same size, or
 * ordered from latest to smallest.
 *
 * For this implementation the following array sizes will be supported:
 *
 *  16MB, 32MB, 64MB, 128MB, 256MB, 512MB, 1GB, 2GB, 4GB, 8GB
 *
 * A value specified in between will be rounded up to the next higher value.
 *
 *  Array (MB)   DRAM (Mb)   NonSplit/Split  Size (B)    B+R+C  Total Memory
 *  ----------   ---------   --------------  --------    -----  ------------
 *          16          16      NonSplit         16         20       64MB
 *          32          16      NonSplit         32         20      128MB
 *          64          16       Split           32         20      256MB
 *         128          64       Split           32         21      512MB
 *         256         128       Split           32         22        1GB
 *         512         128       Split           32         23        2GB
 *        1024         128       Split           32         24        4GB
 *        2048         128       Split           32         25        8GB
 *        4096         256       Split           32         26       16GB
 *        8192         256       Split           32         27       32GB
 *
 * Bits required for each Array Size:
 *
 *  Array (MB)  Bits Needed     Num Blocks
 *  ----------  ----------- ----------------
 *          16  24  <23:0>     262144 (256K)
 *          32  25  <24:0>     524288 (512K)
 *          64  26  <25:0>    1048576 (1M)
 *         128  27  <26:0>    2097152 (2M)
 *         256  28  <27:0>    4194304 (4M)
 *         512  29  <28:0>    8388608 (8M)
 *        1024  30  <29:0>   16777216 (16M)
 *        2048  31  <30:0>   33554432 (32M)
 *        4096  32  <31:0>   67108864 (64M)
 *        8192  33  <32:0>  134217728 (128M)
 *
 * The AARx Array Address Register will be initialized as follows:
 *
 *  Array (MB)      ADDR    ASIZ TSA  SA  ROWS    BNKS
 *               <34:24> <15:12> <9> <8> <3:2>   <1:0>
 *  ----------   ------- ------- --- --- -----   -----
 *          16              0001   0   0     0       0
 *          32              0010   0   0     0       0
 *          64              0011   0   1     0       0
 *         128              0100   0   1     1       1
 *         256              0101   0   1     1       1
 *         512              0110   0   1     1       1
 *        1024              0111   0   1     1       1
 *        2048              1000   0   1     2       1
 *        4096              1001   0   1     2       2
 *        8192              1010   0   1     2       2
 */
#define AXP_MEMORY_BUFFER_LEN  (AXP_DATA_BLOCK_SIZE / sizeof(u64))

/*
 * Memory is addressable in chunks of 64 byte blocks, but the number of these
 * blocks is configurable.  The following typedef defines a single addressable
 * unit of memory (memory is byte addressable, but are only fetched and stored
 * in 64 byte blocks (except possibly for DMA writes).
 */
typedef u64 AXP_21274_MemoryUnit[AXP_COMMON_BUFFER_SIZE];

 typedef enum
 {
     MemNoOp, ReadMem, WriteMem
 } AXP_21274_Mem_Op;

typedef struct
{
    AXP_21274_MemoryUnit *memBlks;  /* Array Memory */
    u64 *id;                        /* store write and supply read id */
    AXP_21274_Mem_Op request;       /* Read or Write request to array */

    /*
     * The following are used for requests from Cchip to a memory array
     */
    pthread_mutex_t toArrayMutex;
    pthread_cond_t toArrayCond;

    /*
     * The following are used to indicate to the Cchip that the memory array is
     * ready for another request
     */
    pthread_mutex_t arrayReadyMutex;
    pthread_cond_t arrayReadyCond;

    /*
     * The following are used to indicate when the data for a read is ready
     */
    pthread_mutex_t dataReadyMutex;
    pthread_cond_t dataReadyCond;

    /*
     * The following are used to indicate when the buffer is ready for the next
     * read.
     */
    pthread_mutex_t bufferReadyMutex;
    pthread_cond_t bufferReadyCond;

    u64 arraySize;      /* Array size in bytes */
    u64 baseAddress;    /* Lowest valid address for array */
    u64 maxAddress;     /* Highest valid address for array */
    u64 address;        /* Address for read/write request */

    bool dataReady;     /* Data for read is ready */
    bool arrayReady;    /* Array ready to have another request queued up */
} AXP_21274_MemArray;

typedef struct
{
    AXP_21274_MemArray arrays[AXP_COMMON_MAX_ARRAYS];
    u64 memSize;
    u32 arrayCount;
    pthread_t arrayThreads[AXP_COMMON_MAX_ARRAYS];
} AXP_21274_Memory;

#endif /* _MEMORY_AXP_MEMORY_H_ */
