#
# Copyright (C) Jonathan D. Belanger 2020.
# All Rights Reserved.
#
# This software is furnished under a license and may be used and copied only
# in accordance with the terms of such license and with the inclusion of the
# above copyright notice.  This software or any other copies thereof may not
# be provided or otherwise made available to any other person.  No title to
# and ownership of the software is hereby transferred.
#
# The information in this software is subject to change without notice and
# should not be construed as a commitment by the author or co-authors.
#
# The author and any co-authors assume no responsibility for the use or
# reliability of this software.
#
# Description:
#
#   This CMake file is used to build the libraries and executables for the
#   DECaxp project.
#
# Revision History:
#   V01.000 03-Jan-2020 Jonathan D. Belanger
#   Initially written.
#
add_library(system STATIC
    AXP_21274_AddressMapping.c
    AXP_21274_System.c)

target_include_directories(system PRIVATE
    ${PROJECT_SOURCE_DIR}/src)
