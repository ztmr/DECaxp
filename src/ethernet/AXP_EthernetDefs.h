/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the definitions to allow the emulator to use one
 *  or more ethernet devices and send and receive packets over then for
 *  specific MAC addresses.
 *
 * Revision History:
 *
 *  V01.000 04-Jan-2018 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_ETHERNET_DEFS_H_
#define _AXP_ETHERNET_DEFS_H_

#include "ethernet/AXP_Ethernet.h"

/*
 * Function prototypes.
 */
AXP_Ethernet_Handle*
AXP_EthernetOpen(char*, u8);
void
AXP_EthernetClose(AXP_Ethernet_Handle*);

#endif /* _AXP_ETHERNET_H_ */
