/*
 * Copyright (C) Jonathan D. Belanger 2018-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the definitions required for the Tsunami/Typhoon
 *  Pchip emulation.
 *
 * Revision History:
 *
 *  V01.000 22-Mar-2018 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.001 12-May-2018 Jonathan D. Belanger
 *  Moved the Pchip CSRs and queues over here to be similar to the real thing.
 *
 *  V01.002 22-Dec-2019 Jonathan D. Belanger
 *  Reorganizing the code so that header files and source files are in the same
 *  directory.
 */
#ifndef _AXP_21274_PCHIP_H_
#define _AXP_21274_PCHIP_H_

#include "com-util/AXP_Utility.h"
#include "com-util/AXP_CAPbus.h"
#include "system/AXP_21274_Registers.h"
#include "c-chip/AXP_21274_Cchip.h"

/*
 * Translation Lookaside Buffer
 */
typedef struct
{
        bool dacCycle;
        u32 pciAddr;
} AXP_21274_TAG;
typedef struct
{
    bool valid;
    u64 cpuPageAddr;
} AXP_21274_DATA;
typedef struct
{
    AXP_21274_TAG tag;
    AXP_21274_DATA data[4];
} AXP_21274_TLB;

/*
 * Control commands for the Pchip come from the Cchip, but data is transferred
 * between the Pchip and Dchip.  This data to the Dchip is on its way to one of
 * a CPU, memory, or the other Pchip.
 *
 * An Alpha AXP system can have 1 or 2 Pchips.  The Pchips communicate with
 * the Cchip and Dchip.  A high-level diagram would look like the following
 * (from a Pchip perspective):
 *
 *         CAP Cmd     +---------+
 *       +------------>+ Pchip 0 +<------------+
 *       |             +----+----+             | Data
 *       |                  ^                  |
 *       |                  | Cmd/Data         v            +------+
 *       v                  v             +----+----+   +-->+ CPUs |
 *  +----+----+      +------+------+      |         +<--+   +------+
 *  |  Cchip  |      | PCI Devices |      |  Dchip  |
 *  +----+----+      +------+------+      +         +<--+   +--------+
 *       ^                  ^             +----+----+   +-->+ Memory |
 *       |                  | Cmd/Data         ^            +--------+
 *       |                  v                  |
 *       |             +----+----+             | Data
 *       +------------>+ Pchip 1 +<------------+
 *         CAP Cmd     +---------+
 *
 * As you can see the data from the PCI Devices go through the Pchip on its way
 * to some place beyond the Dchip.  Data and commands to and from the Pchip are
 * the responsibility of the Pchip, as well.
 *
 * For data coming into the Pchip, through the Dchip, the data is retrieved
 * from the Dchip when the Cchip instructs the Pchip to do so, and the Pchip
 * will process it or send it to the PCI devices.
 *
 * For data going out to the System, also through the Dchip, the Cchip needs to
 * know when the data get is ready for the Cchip to be able to instruct the
 * Dchip to retrieve and process it.
 *
 * HRM: 8.1 Pchip Architecture
 *
 *  The Pchip is the interface between devices on the PCI bus and the rest of
 *  the system.  There can be one or two Pchips, and corresponding single or
 *  dual PCI buses, connected to the Cchip and Dchips.  The Pchip performs the
 *  following functions:
 *
 *      - Accepts requests from the Cchip by means of the CAPbus and enqueues
 *        them
 *      - Issues commands to the PCI bus based on these requests
 *      - Accepts requests from the PCI bus and enqueues them
 *      - Issues commands to the Cchip by means of the CAPbus based on these
 *        requests
 *      - Transfers data to and from the Dchips based on the above commands and
 *        requests
 *      - Buffers the data when necessary
 *      - Reports errors to the Cchip, after recoding the nature of the error
 */

/*
 * The following definition contains the fields and data structures required to
 * implement a single Pchip.  There is always at least one of these and as many
 * as two of them.
 */
#define AXP_21274_PCHIP_RQ_SIZE    4 /* up to 4 messages from Cchip */
typedef struct
{

    /*
     * There is one thread/mutex/condition variable per Pchip.
     */
    pthread_t threadID;
    pthread_mutex_t mutex;
    pthread_cond_t cond;

    /*
     * Pchip ID (either 0 or 1)
     */
    u32 pChipID;

    /*
     * Interface queues.
     */
    AXP_CAP_Msg prq[AXP_21274_PCHIP_RQ_SIZE];   /* CSC<PRQMAX> */
    u32 prqCnt;                                 /* CSC<PRQMAX> */
    u32 rqIdx;
    AXP_QUEUE_HDR padBus;
    AXP_21274_SYSTEM *cChip;

    /*
     * PCI Bus queues
     */
    AXP_QUEUE_HDR qddw; /* Downstream Write Data Queue - 16 QW */
    AXP_QUEUE_HDR qddr; /* Downstream Read Data Queue - 16 QW */
    AXP_QUEUE_HDR qdaw; /* Downstream Write Address Queue - 4 */
    AXP_QUEUE_HDR qdar; /* Downstream Read Address Queue - 4 */
    AXP_QUEUE_HDR qudw; /* Upstream Write Data Queue - 16 QW */
    AXP_QUEUE_HDR qudr; /* Upstream Read Data Queue - 16 QW */

    /*
     * PTP and DMA
     */
    AXP_QUEUE_HDR ptpRW;
    AXP_QUEUE_HDR dmaWrite[2];
    AXP_QUEUE_HDR dmaRead;

    /*
     * Scatter-Gather Associative TLB
     */
    AXP_21274_TLB tlb[168];

    /*
     * The following are the Pchip CSRs.  The addresses for these Pchip CSRs
     * have n=1 for p0 and n=3 for p1.
     */
    AXP_21274_WSBAn wsba0;          /* Address: 80n.8000.0000 */
    AXP_21274_WSBAn wsba1;          /* Address: 80n.8000.0040 */
    AXP_21274_WSBAn wsba2;          /* Address: 80n.8000.0080 */
    AXP_21274_WSBA3 wsba3;          /* Address: 80n.8000.00c0 */
    AXP_21274_WSMn wsm0;            /* Address: 80n.8000.0100 */
    AXP_21274_WSMn wsm1;            /* Address: 80n.8000.0140 */
    AXP_21274_WSMn wsm2;            /* Address: 80n.8000.0180 */
    AXP_21274_WSMn wsm3;            /* Address: 80n.8000.01c0 */
    AXP_21274_TBAn tba0;            /* Address: 80n.8000.0200 */
    AXP_21274_TBAn tba1;            /* Address: 80n.8000.0240 */
    AXP_21274_TBAn tba2;            /* Address: 80n.8000.0280 */
    AXP_21274_TBAn tba3;            /* Address: 80n.8000.02c0 */
    AXP_21274_PCTL pctl;            /* Address: 80n.8000.0300 */
    AXP_21274_PLAT plat;            /* Address: 80n.8000.0340 */
 /* AXP_21274_RES        res;        * Address: 80n.8000.0380 */
    AXP_21274_PERROR perror;        /* Address: 80n.8000.03c0 */
    AXP_21274_PERRMASK perrMask;    /* Address: 80n.8000.0400 */
    AXP_21274_PERRSET perrSet;      /* Address: 80n.8000.0440 */
    AXP_21274_TLBIV tlbiv;          /* Address: 80n.8000.0480 */
    AXP_21274_PMONCTL pMonCtl;      /* Address: 80n.8000.0500 */
    AXP_21274_PMONCNT pMonCnt;      /* Address: 80n.8000.0540 */
    AXP_21274_SPRST sprSt;          /* Address: 80n.8000.0800 */
} AXP_21274_PCHIP;

#define AXP_21274_WHICH_PCHIP(addr) (((addr) & 0x0000000200000000) >> 33)

#endif /* _AXP_21274_PCHIP_H_ */
