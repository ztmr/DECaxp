/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the definitions required for the Tsunami/Typhoon
 *  Pchip emulation.
 *
 * Revision History:
 *
 *  V01.000 04-Jan-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_21274_PCHIP_DEFS_H_
#define _AXP_21274_PCHIP_DEFS_H_

#include "p-chip/AXP_21274_Pchip.h"

/*
 * Pchip Function Prototypes
 */
void
AXP_21274_Pchip_Init(AXP_21274_PCHIP *p, u32 id);

void*
AXP_21274_PchipMain(void*);

#endif /* _AXP_21274_PCHIP_DEFS_H_ */
