/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *  This is the header file used to define the prototypes for the TOY Clock.
 *
 * Revision History:
 *
 *  V01.000 04-May-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_DS12887A_TOYCLOCK_DEFS_
#define _AXP_DS12887A_TOYCLOCK_DEFS_

#include "com-util/AXP_Common_Include.h"

/*
 * External prototypes.
 */
void
AXP_DS12887A_Reset(void);

void
AXP_DS12887A_Write(u8 addr, u8 value);

void
AXP_DS12887A_Read(u8 addr, u8 *value);

void
AXP_DS12887A_Config(pthread_cond_t *cond, pthread_mutex_t *mutex,
                    u64 *irq_field, u64 irq_mask, bool dstIsEurpoean);

#endif  /* _AXP_DS12887A_TOYCLOCK_DEFS_ */
