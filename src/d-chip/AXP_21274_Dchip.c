/*
 * Copyright (C) Jonathan D. Belanger 2018-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  The Dchip performs the following functions:
 *      - Implements data flow between the Pchips, CPUs, and memory
 *      - Shifts data to and from the PADbus, as required
 *      - Provides Pchip queue buffering
 *      - Provides memory data buffering
 *      - Implements data merging for quadword write operations to memory and
 *        the DMA RMW command
 *
 * Dchip architecture does not implement:
 *      - Flow control
 *      - Error detection
 *      - Error reporting
 *      - Error correction
 *      - Data wrapping
 *
 * The Dchip uses multiplexers to switch data among its ports and queues. In
 * addition to moving data from one port to another, these multiplexers must
 * support the various system configurations. The system may have two, four, or
 * eight Dchips. This allows for one or two 21264 CPUs, one or two Pchip ports,
 * and one or two 16-byte or 32-byte memory buses. Data may be moved between
 * the CPU, Pchips, or memory ports. Also, data may be transferred between the
 * two or up to 4 CPU ports. PTP transfers are supported between Pchip ports.
 *
 * Revision History:
 *
 *  V01.000 22-Mar-2018 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.001 22-Dec-2019 Jonathan D. Belanger
 *  Reorganizing the code so that header files and source files are in the same
 *  directory.
 *
 *  V01.002 31-Dec-2019 Jonathan D. Belanger
 *  Started to completely rewrite the Dchip code.
 */
#include "com-util/AXP_System_Common.h"
#include "com-util/AXP_SysDataDefs.h"
#include "com-util/AXP_Blocks.h"
#include "system/AXP_21274_System.h"
#include "d-chip/AXP_21274_DchipDefs.h"

/*
 * AXP_21274_Dchip_Init
 *  This function is called to initialize the Dchip CSRs as documented in HRM
 *  10.2 Chipset Registers.
 *
 * Input Parameters:
 *  sys:
 *      A pointer to the system data structure from which the emulation
 *      information is maintained.
 *
 * Output Parameters:
 *  dChip:
 *      A pointer to the Dchip data structure, with the Dchip CSRs initialized
 *      based on the HRM for Tsunami/Typhoon chip set.
 *
 * Return Values:
 *  None.
 */
void
AXP_21274_Dchip_Init(AXP_21274_DCHIP *dChip)
{
    int ii;

    /*
     * Initialize the Memory buffers.  Then let the Memory module know about
     * their address so that the Dchip and Memory can transfer data between
     * them.  There are 4 memory arrays, each with one read and one write
     * buffer.
     */
    for (ii = 0; ii < AXP_COMMON_MAX_ARRAYS; ii++)
    {
        dChip->memoryIn[ii] = AXP_SysData_Create(1);
        dChip->memoryOut[ii] = AXP_SysData_Create(1);
    }

    /*
     * Initialize the PCI buffers.
     */
    for (ii = 0; ii < AXP_COMMON_MAX_PCHIPS; ii++)
    {
        dChip->pciIn[ii] = AXP_SysData_Create(1);
        dChip->pciOut[ii] = AXP_SysData_Create(1);
    }

    /*
     * Initialize the CPU buffers.
     */
    for (ii = 0; ii < AXP_COMMON_MAX_CPUS; ii++)
    {
        dChip->cpuIn[ii] = AXP_SysData_Create(AXP_COMMON_BUFS_PER_CPU);
        dChip->cpuOut[ii] = AXP_SysData_Create(AXP_COMMON_BUFS_PER_CPU);
    }

    /*
     * Initialize the Cchip buffers.
     *
     * The following definitions are not part of the real architecture.  Having
     * to send a buffer to one of the Pchips and then have it forward it on to
     * the Dchip, and vice versa, for data to get in and out of the Cchip, just
     * seemed inefficient.  We only require one additional buffer in each
     * direction.
     */
    dChip->cChipIn = AXP_SysData_Create(1);
    dChip->cChipOut = AXP_SysData_Create(1);

    /*
     * Initialize the Dchip internal queues.
     */
    dChip->wmb = AXP_SysData_Create(AXP_DCHIP_MAX_WMB_BUFS);
    dChip->tpqm = AXP_SysData_Create(AXP_DCHIP_MAX_xPQ_BUFS);
    dChip->tpqp = AXP_SysData_Create(AXP_DCHIP_MAX_xPQ_BUFS);
    dChip->fpq = AXP_SysData_Create(AXP_DCHIP_MAX_xPQ_BUFS);

    /*
     * Initialization for DSC (HRM Table 10-31)
     */
    dChip->dsc.res_7 = 0;
    dChip->dsc.p1p = 0;   /* Pchip 1 present from CPM command from Cchip */
    dChip->dsc.c3cfp = 0; /* CPU3 clock forward from CPM command from Cchip */
    dChip->dsc.c2cfp = 0; /* CPU2 clock forward from CPM command from Cchip */
    dChip->dsc.c1cfp = 0; /* CPU1 clock forward from CPM command from Cchip */
    dChip->dsc.c0cfp = 0; /* CPU0 clock forward from CPM command from Cchip */
    dChip->dsc.bc = 0;    /* Base Configuration from CPM command from Cchip */

    /*
     * The first byte is replicated 8 times.
     */
    dChip->dsc.dchip7 = dChip->dsc.dchip6 = dChip->dsc.dchip5
            = dChip->dsc.dchip4 = dChip ->dsc.dchip3 = dChip->dsc.dchip2
            = dChip->dsc.dchip1 = dChip->dsc.dchip0;

    /*
     * Initialization for DSC2 (HRM Table 10-32)
     */
    dChip->dsc2.res_5 = 0;
    dChip->dsc2.res_2 = 0;
    dChip->dsc2.p1w = 0;
    dChip->dsc2.p0w = 0;

    /*
     * Initialization for STR (HRM Table 10-33)
     */
    dChip->str.res_7 = 0;
    dChip->str.iddw = 2;
    dChip->str.iddr = 4;
    dChip->str.aw = 0;

    /*
     * The first byte is replicated 8 times.
     */
    dChip->str.dchip7 = dChip->str.dchip6 = dChip->str.dchip5
            = dChip->str.dchip4 = dChip->str.dchip3 = dChip->str.dchip2
            = dChip->str.dchip1 = dChip->str.dchip0;

    /*
     * Initialization for DREV (HRM Table 10-34)
     */
    dChip->dRev.res_60 = 0;
    dChip->dRev.rev7 = 1;
    dChip->dRev.res_52 = 0;
    dChip->dRev.rev6 = 1;
    dChip->dRev.res_44 = 0;
    dChip->dRev.rev5 = 1;
    dChip->dRev.res_36 = 0;
    dChip->dRev.rev4 = 1;
    dChip->dRev.res_28 = 0;
    dChip->dRev.rev3 = 1;
    dChip->dRev.res_20 = 0;
    dChip->dRev.rev2 = 1;
    dChip->dRev.res_12 = 0;
    dChip->dRev.rev1 = 1;
    dChip->dRev.res_4 = 0;
    dChip->dRev.rev0 = 1;

    /*
     * Return back to the caller.
     */
    return;
}

/*
 * AXP_21274_Dchip_CPM
 *  This function is called by the Cchip to move data from one internal queue
 *  to another based on the supplied command.
 *
 * Input Parameters:
 *  dChip:
 *      A pointer to the Dchip data structure from which the Dchip emulation
 *      information is maintained.
 *  cmd:
 *      A value indicating the CPM command to be processed.
 *  cpu:
 *      Depending upon the value of cmd, this parameter will contain an
 *      indication of the CPU to or from which data is to be moved.
 *  mem:
 *      Depending upon the value of cmd, this parameter will contain an
 *      indication of the Memory array to or from which data is to be moved.
 *  csr:
 *      Depending upon the value of cmd, this parameter will contain an
 *      indication of the Dchip CSR data that is to be transferred.
 *  id:
 *      A pointer to the value of the buffer in the SysData queue that needs to
 *      be swapped or removed.
 *
 * Output Parameters:
 *  id:
 *      A pointer to a 64-bit location to receive the id associated with the
 *      SysData buffer in the target queue.
 *
 * Return Values:
 *  None.
 */
void
AXP_21274_Dchip_CPM(AXP_21274_DCHIP *dChip, AXP_CPM_Command cmd, u8 cpu,
                    u8 mem, u8 csr, u64* id)
{
    AXP_SysData_Buffer buf;
    u32 len;
    bool status;    /* TODO: What to do with this */

    switch (cmd)
    {

        /*
         * There is nothing that Dchip needs to do, so we can just return.
         */
        case CPM_NoOp:
            break;

        /*
         * Memory bus 'mem' to CPU bus 'cpu'
         */
        case CPM_m2c:
            status = AXP_SysData_Swap(dChip->memoryIn[mem], *id,
                                      dChip->cpuOut[cpu]);
            break;

        /*
         * Cchip CSR to CPU bus 'cpu'
         */
        case CPM_csr2c:
            status = AXP_SysData_Swap(dChip->cChipIn, *id, dChip->cpuOut[cpu]);
            break;

        /*
         * CPU bus 'cpu' to Cchip CSR
         */
        case CPM_c2csr:
            status = AXP_SysData_Swap(dChip->cpuIn[cpu], *id, dChip->cChipOut);
            break;

        /*
         * CPU bus 'cpu' to Memory bus 'mem'
         */
        case CPM_c2m:
            status = AXP_SysData_Swap(dChip->cpuIn[cpu], *id,
                                      dChip->memoryOut[mem]);
            break;

        /*
         * Memory bus 'mem' to dChip->tpqm
         */
        case CPM_m2p:
            status = AXP_SysData_Swap(dChip->tpqm, *id, dChip->memoryIn[mem]);
            break;

        /*
         * FPQ to Memory bus 'mem'
         */
        case CPM_pw2m:
            status = AXP_SysData_Swap(dChip->fpq, *id, dChip->memoryOut[mem]);
            break;

        /*
         * TIGbus to CPU bus 'cpu'
         *
         * TODO: Determine if the buffer from or to the CPU is counted in bytes
         *       or quadwords.
         * TODO: Also need to determine if TIGbus data is always in cache block
         *       sizes (I think yes, as we are generally loading the Icache).
         */
        case CPM_t2c:
            status = AXP_SysData_Swap(dChip->cChipIn, *id, dChip->cpuOut[cpu]);
            break;

        /*
         * CPU bus 'cpu' to TIGbus
         *
         * TODO: Determine if the buffer from or to the CPU is counted in bytes
         *       or quadwords.
         * TODO: Also need to determine if TIGbus data is always in cache block
         *       sizes (I think yes, as we are generally loading the Icache).
         */
        case CPM_c2t:
            status = AXP_SysData_Swap(dChip->cpuIn[cpu], *id, dChip->cChipOut);
            break;

        /*
         * CPU bus 'cpu' to TPQP
         *
         * I/O Writes can have lengths from 1 byte to 8 quadwords.  The SysDc
         * command determine how many and of what size.  The following table
         * describes the SysDc command and mask that are used to determine what
         * the actual length will be (in bytes):
         *
         *                               Mask
         *  ----------+---------------------------------------------
         *  Command   | 01    03    07    0F    1F    3F    7F    FF
         *  ----------+---------------------------------------------
         *  WrBytes   |  1     2     3     4     5     6     7     8
         *  WrLWs     |  4     8    12    16    20    24    28    32
         *  WrQWs     |  8    16    24    32    40    48    56    64
         *  ----------+---------------------------------------------
         *
         * TODO: Need to determine the number of bytes actually being
         *       transferred to the PCI device.
         */
        case CPM_c2p:
            status = AXP_SysData_Swap(dChip->cpuIn[cpu], *id, dChip->tpqp);
            break;

        /*
         * - Cache hit on CPU 'cpu' to TPQP.
         * - Load cache data at top of TPQP and overwrite stale memory data
         *
         * I/O Writes can have lengths from 1 byte to 8 quadwords.  The SysDc
         * command determine how many and of what size.  The following table
         * describes the SysDc command and mask that are used to determine what
         * the actual length will be (in bytes):
         *
         *                               Mask
         *  ----------+---------------------------------------------
         *  Command   | 01    03    07    0F    1F    3F    7F    FF
         *  ----------+---------------------------------------------
         *  WrBytes   |  1     2     3     4     5     6     7     8
         *  WrLWs     |  4     8    12    16    20    24    28    32
         *  WrQWs     |  8    16    24    32    40    48    56    64
         *  ----------+---------------------------------------------
         *
         * TODO: This needs work (what does overwrite stale memory mean)
         */
        case CPM_h2po:
            status = AXP_SysData_Swap(dChip->tpqp, *id, dChip->cpuIn[cpu]);
            break;

        /*
         * - Late cache hit on CPU 'cpu' to TPQP.
         * - Decrement top of TPQP pointer, then load late cache data at top of
         *   dChip->tpqp and overwrite stale memory data
         *
         * I/O Writes can have lengths from 1 byte to 8 quadwords.  The SysDc
         * command determine how many and of what size.  The following table
         * describes the SysDc command and mask that are used to determine what
         * the actual length will be (in bytes):
         *
         *                               Mask
         *  ----------+---------------------------------------------
         *  Command   | 01    03    07    0F    1F    3F    7F    FF
         *  ----------+---------------------------------------------
         *  WrBytes   |  1     2     3     4     5     6     7     8
         *  WrLWs     |  4     8    12    16    20    24    28    32
         *  WrQWs     |  8    16    24    32    40    48    56    64
         *  ----------+---------------------------------------------
         *
         * TODO: This needs work (what does overwrite stale memory mean)
         */
        case CPM_h2pb:
            status = AXP_SysData_Swap(dChip->cpuIn[cpu], *id, dChip->tpqp);
            break;

        /*
         * CPU bus 'cpu' to Dchip CSR 'csr'
         *
         * The only writable CSR in the Dchip is the System Timing Register
         * (STR).
         */
        case CPM_c2dp:
            AXP_SysData_Remove(dChip->cpuOut[cpu], *id, buf, &len);
            switch (csr)
            {
                case AXP_DCHIP_STR_NUM:
                    *(u64 *) &dChip->str = buf[0];
                    break;

                default:
                    break;
            }
            break;

        /*
         * Dchip CSR 'csr' to CPU bus 'cc'
         */
        case CPM_dp2c:
            switch (csr)
            {
                case AXP_DCHIP_DSC_NUM:
                    buf[0] = *(u64 *) &dChip->dsc;
                    break;

                case AXP_DCHIP_STR_NUM:
                    buf[0] = *(u64 *) &dChip->str;
                    break;

                case AXP_DCHIP_DREV_NUM:
                    buf[0] = *(u64 *) &dChip->dRev;
                    break;

                case AXP_DCHIP_DSC2_NUM:
                    buf[0] = *(u64 *) &dChip->dsc2;
                    break;

                default:
                    buf[0] = 0;
                    break;
            }
            *id = AXP_SysData_Add(dChip->cpuOut[cpu], buf, sizeof(u64));
            break;

        /*
         * FPQ to CPU bus 'cpu'
         *
         * I/O Reads can have lengths from 1 byte to 8 quadwords.  The SysDc
         * command determine how many and of what size.  The following table
         * describes the SysDc command and mask that are used to determine what
         * the actual length will be (in bytes):
         *
         *                               Mask
         *  ----------+---------------------------------------------
         *  Command   | 01    03    07    0F    1F    3F    7F    FF
         *  ----------+---------------------------------------------
         *  ReadBytes |  1     2     3     4     5     6     7     8
         *  ReadLWs   |  4     8    12    16    20    24    28    32
         *  ReadQWs   |  8    16    24    32    40    48    56    64
         *  ----------+---------------------------------------------
         *
         * TODO: Need to determine the number of bytes actually being
         *       transferred from the PCI device.
         */
        case CPM_p2c:
            status = AXP_SysData_Swap(dChip->fpq, *id, dChip->cpuOut[cpu]);
            break;

        /*
         * FPQ to TPQP (PTP operations)
         */
        case CPM_p2p:
            status = AXP_SysData_Swap(dChip->fpq, *id, dChip->tpqp);
             break;

        /*
         * CPU bus 'cpu' to CPU bus 0
         */
        case CPM_h2c0:
            status = AXP_SysData_Swap(dChip->cpuIn[cpu], *id,
                                      dChip->cpuOut[0]);
            break;

        /*
         * CPU bus 'cpu' to CPU bus 1
         */
        case CPM_h2c1:
            status = AXP_SysData_Swap(dChip->cpuIn[cpu], *id,
                                      dChip->cpuOut[1]);
            break;

        /*
         * CPU bus 'cpu' to CPU bus 2
         */
        case CPM_h2c2:
            status = AXP_SysData_Swap(dChip->cpuIn[cpu], *id,
                                      dChip->cpuOut[2]);
            break;

        /*
         * CPU bus 'cpu' to CPU bus 3
         */
        case CPM_h2c3:
            status = AXP_SysData_Swap(dChip->cpuIn[cpu], *id,
                                      dChip->cpuOut[3]);
            break;

        /*
         * Memory bus 'mem' to dChip->wmb
         */
        case CPM_m2w:
            status = AXP_SysData_Swap(dChip->memoryOut[mem], *id, dChip->wmb);
            break;

        /*
         * WMB to memory bus 'mem'
         */
        case CPM_w2m:
            status = AXP_SysData_Swap(dChip->wmb, *id, dChip->memoryIn[mem]);
            break;

        /*
         * Cache hit on CPU 'cpu' to WMB
         */
        case CPM_h2w:
            status = AXP_SysData_Swap(dChip->cpuIn[cpu], *id, dChip->wmb);
            break;
    }
    return;
}

/*
 * AXP_21274_Dchip_PAD
 *  This function is called by the Cchip to move data to or from the Pchip.
 *
 * Input Parameters:
 *  dChip:
 *      A pointer to the Dchip data structure from which the Dchip emulation
 *      information is maintained.
 *  cmd:
 *      A value indicating the PAD command to be processed.
 *  pchip:
 *      A value indicating which Pchip is being addressed.
 *  shift:
 *      A value indicating the shift of the data to be performed.
 *  len:
 *      A value indicating the length of the data, in quadwords, to be moved
 *  id:
 *      The value of the buffer in the SysData queue that needs to be swapped.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
void
AXP_21274_Dchip_PAD(AXP_21274_DCHIP *dChip, AXP_PAD_Command cmd, u8 pchip,
                    AXP_PAD_Shift shift, AXP_PAD_Length len, u64 id)
{
    bool status;    /* TODO: What to do with this */

    /*
     * TODO: Need to handle the shift functionality.
     */
    switch (cmd)
    {

        /*
         * All PAD commands with the high bit not set are considered NoOps,
         * therefore, just return back to the caller.
         */
        case PAD_NoOp0:
        case PAD_NoOp1:
        case PAD_NoOp2:
        case PAD_NoOp3:
        case PAD_NoOp4:
        case PAD_NoOp5:
        case PAD_NoOp6:
        case PAD_NoOp7:
            break;

        /*
         * Move data from Pchip 'pchip' to the FPQ
         */
        case PAD_P_FPQ:
            status = AXP_SysData_Swap(dChip->pciIn[pchip], id, dChip->fpq);
            break;

        /*
         * Move data from TPQM to Pchip 'pchip'
         */
        case PAD_TPQM_P:
            status = AXP_SysData_Swap(dChip->tpqm, id, dChip->pciOut[pchip]);
            break;

        /*
         * Move data from Pchip 'pchip' to WMB
         */
        case PAD_P_WMB:
            status = AXP_SysData_Swap(dChip->pciIn[pchip], id, dChip->wmb);
            break;

        /*
         * Move data from WMB to Pchip 'pchip'
         */
        case PAD_WMB_P:
            status = AXP_SysData_Swap(dChip->wmb, id, dChip->pciOut[pchip]);
            break;

        /*
         * Stutter move of data from Pchip 'pchip' to FPQ.
         *
         * TODO: This PAD command is used for PIO Read Byte and PIO Read
         *       Longword, and needs to take this into consideration.
         */
        case PAD_PP_FPQ:
            status = AXP_SysData_Swap(dChip->pciIn[pchip], id, dChip->fpq);
            break;

        /*
         * Move data from TPQP to Pchip 'pchip'
         */
        case PAD_TPQP_P:
            status = AXP_SysData_Swap(dChip->tpqp, id, dChip->pciOut[pchip]);
            break;
    }

    return;
}
