/*
 * Copyright (C) Jonathan D. Belanger 2017-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 10-May-2017 Jonathan D. Belanger
 *  Initially written.
 *
 *  V01.001 14-May-2017 Jonathan D. Belanger
 *  Added includes for a number of standard header files.
 *
 *  V01.002 17-Jun-2017 Jonathan D. Belanger
 *  Added counted queues and counted queue entries.
 *
 *  V01.003 05-Jul-2017 Jonathan D. Belanger
 *  Added definitions for unsigned/signed 128-bit values.
 *
 *  V01.004 10-Jul-2017 Jonathan D. Belanger
 *  I tried to utilize the MPFR - Multi-Precision Float Rounding utility,
 *  but had problems getting it to link in correctly.  Additionally, this
 *  utility did not support subnormal values directly (there was a way to
 *  emulate it, but was not very efficient), as well as the performance
 *  of this utility has been questioned.  Therefore, I'm reverting to what
 *  I was doing before.
 *
 *  V01.005 18-Nov-2017 Jonathan D. Belanger
 *  Uncommented the pthread.h include.  We are going threading.
 *
 *  V01.006 29-Dec-2017 Jonathan D. Belanger
 *  When sending data from the CPU to the System, we do so in up to 64-byte
 *  blocks.  Since these blocks are not necessarily consecutive, there may be
 *  gaps between the end of what section of relevant data and the start of the
 *  next.  In the real CPU, this would be broken down into a series of
 *  WrBytes/WrLWs/WrQWs or ReadBytes/ReadLWs/ReadQWs.  We need to mimic this,
 *  but do it in a single step.  We need some macros to set/decode the
 *  appropriate set of mask bits (each bit represent a single byte).
 *
 *  V01.007 26-Apr-2018 Jonathan D. Belanger
 *  Added macros to INSQUE and REMQUE entries from a doubly linked list.
 *
 *  V01.008 22-Dec-2019 Jonathan D. Belanger
 *  Reorganizing the code so that header files and source files are in the same
 *  directory.
 *
 *  V01.009 03-Jan-2020 Jonathan D. Belanger
 *  Moving the common includes to their own header file.  This way we are not
 *  mixing general definitions with other structure and prototype definitions.
 */
#ifndef _AXP_UTIL_DEFS_
#define _AXP_UTIL_DEFS_

#include "com-util/AXP_Common_Include.h"

/*
 * Define the SROM handle for reading in an SROM file.
 */
#define AXP_FILENAME_MAX    131
typedef struct
{
    char fileName[AXP_FILENAME_MAX + 1];
    u32 *writeBuf;
    FILE *fp;
    u32 validPat; /* must be 0x5a5ac3c3 */
    u32 inverseVP; /* must be 0xa5a53c3c */
    u32 hdrSize;
    u32 imgChecksum;
    u32 imgSize; /* memory footprint */
    u32 decompFlag;
    u64 destAddr;
    u8 hdrRev;
    u8 fwID; /* Firmware ID */
    u8 hdrRevExt;
    u8 res;
    u32 romImgSize;
    u64 optFwID; /* Optional Firmware ID */
    u32 romOffset;
    u32 hdrChecksum;
    u32 verImgChecksum;
    bool romOffsetValid;
    bool openForWrite;
} AXP_SROM_HANDLE;

#define AXP_ROM_HDR_LEN     (14*4)
#define AXP_ROM_HDR_CNT     15          /* romOffset[Valid] read as one */
#define AXP_ROM_VAL_PAT     0x5a5ac3c3
#define AXP_ROM_INV_VP_PAT  0xa5a53c3c
#define AXP_ROM_HDR_VER     1
#define AXP_ROM_FWID_DBM    0           /* Alpha Motherboards Debug Monitor firmware */
#define AXP_ROM_FWID_WNT    1           /* Windows NT firmware */
#define AXP_ROM_FWID_SRM    2           /* Alpha System Reference Manual Console */
#define AXP_ROM_FWID_FSB    6           /* Alpha Motherboards Fail-Safe Booter */
#define AXP_ROM_FWID_MILO   7           /* Linux Miniloader */
#define AXP_ROM_FWID_VXWRKS 8           /* VxWorks Real-Time Operating System */
#define AXP_ROM_FWID_SROM   10          /* Serial ROM */

/*
 * CPU <--> System Mask generation macro definitions.
 */
#define AXP_MASK_BYTE       0x0001
#define AXP_MASK_WORD       0x0003
#define AXP_MASK_LONG       0x000f
#define AXP_MASK_QUAD       0x00ff

/*
 * Define a basic queue.  This will be used to define a number of other queue
 * types.
 */
typedef struct queueHeader
{
    struct queueHeader *flink;
    struct queueHeader *blink;
} AXP_QUEUE_HDR;

/*
 * Macros to use with the basic queue.
 */
#define AXP_INIT_QUE(queue)                                                 \
    queue.flink = queue.blink = (AXP_QUEUE_HDR*) &queue
#define AXP_INIT_QUEP(queue)                                                \
    queue->flink = queue->blink = (AXP_QUEUE_HDR *) queue
#define AXP_QUE_EMPTY(queue)                                                \
    (queue.flink == (AXP_QUEUE_HDR *) &queue.flink)
#define AXP_QUEP_EMPTY(queue)                                               \
    (queue->flink == (AXP_QUEUE_HDR *) &queue->flink)
#define AXP_REMQUE(entry)                                                   \
    {                                                                       \
        (entry)->blink->flink = (entry)->flink;                             \
        (entry)->flink->blink = (entry)->blink;                             \
    }
#define AXP_INSQUE(pred, entry)                                             \
    {                                                                       \
        (entry)->flink = (pred)->flink;                                     \
        (entry)->blink = (AXP_QUEUE_HDR *) (pred);                          \
        (pred)->flink = (AXP_QUEUE_HDR *) (entry);                          \
        (entry)->flink->blink = (AXP_QUEUE_HDR *) (entry);                  \
    }

/*
 * A counted queue.  If maximum is specified as zero at initialization, then
 * the number of entries in the queue has no limit.
 */
typedef struct countedQueueEntry
{
    struct countedQueueEntry *flink;
    struct countedQueueEntry *blink;
    struct countedQueueRoot *parent;
    int index;
} AXP_CQUE_ENTRY;

typedef struct countedQueueRoot
{
    struct countedQueueEntry *flink;
    struct countedQueueEntry *blink;
    u32 count;
    u32 max;
    pthread_mutex_t mutex;
} AXP_COUNTED_QUEUE;

#define AXP_INIT_CQENTRY(queue, prent)                                      \
    queue.flink = NULL;                                                     \
    queue.blink = NULL;                                                     \
    queue.parent = &prent;
#define AXP_INIT_CQENTRYP(queue, prent)                                     \
    queue->flink = NULL;                                                    \
    queue->blink = NULL;                                                    \
    queue->parent = prent;

/*
 * Conditional queues (using pthreads)
 *
 * Unlike the above, we are not going to use macros to initialize these queues.
 * We'll use function calls.
 */
typedef enum
{
    AXPCondQueue, AXPCountedCondQueue
} AXP_COND_Q_TYPE;

typedef struct condQueueHeader
{
    struct condQueueHeader *flink;
    struct condQueueHeader *blink;
    struct condQueueRoot *parent;
    AXP_COND_Q_TYPE type;
} AXP_COND_Q_HDR;

typedef struct condQueueLeaf
{
    struct condQueueLeaf *flink;
    struct condQueueLeaf *blink;
    struct condQueueRoot *parent;
} AXP_COND_Q_LEAF;

typedef struct condQueueRoot
{
    AXP_COND_Q_LEAF *flink;
    AXP_COND_Q_LEAF *blink;
    struct condQueueRoot *parent;
    AXP_COND_Q_TYPE type;
    pthread_mutex_t qMutex;
    pthread_cond_t qCond;
} AXP_COND_Q_ROOT;

typedef struct condQueueCountRoot
{
    struct condQueueLeaf *flink;
    struct condQueueLeaf *blink;
    struct condQueueCountRoot *parent;
    AXP_COND_Q_TYPE type;
    pthread_mutex_t qMutex;
    pthread_cond_t qCond;
    u32 count;
    u32 max;
} AXP_COND_Q_ROOT_CNT;

/*
 * Error returns for various AXP Utility file load functions.
 */
#define AXP_S_NORMAL        0
#define AXP_E_FNF           -1
#define AXP_E_BUFTOOSMALL   -2
#define AXP_E_EOF           -3
#define AXP_E_READERR       -4
#define AXP_E_BADSROMFILE   -5
#define AXP_E_BADCFGFILE    -6

/*
 * Prototype Definitions
 *
 * Trace Functionality
 */
bool
AXP_TraceInit(void);

/*
 * Least Recently Used (LRU) queue functions.
 */
void
AXP_LRUAdd(AXP_QUEUE_HDR *lruQ, AXP_QUEUE_HDR *entry);
void
AXP_LRURemove(AXP_QUEUE_HDR *entry);
AXP_QUEUE_HDR*
AXP_LRUReturn(AXP_QUEUE_HDR *lruQ);

/*
 * Counted queue functions.
 */
bool
AXP_InitCountedQueue(AXP_COUNTED_QUEUE*, u32);
bool
AXP_LockCountedQueue(AXP_COUNTED_QUEUE*);
bool
AXP_UnlockCountedQueue(AXP_COUNTED_QUEUE*);
i32
AXP_InsertCountedQueue(AXP_CQUE_ENTRY*, AXP_CQUE_ENTRY*);
i32
AXP_CountedQueueFull(AXP_COUNTED_QUEUE*, u32);
i32
AXP_RemoveCountedQueue(AXP_CQUE_ENTRY*, bool);

/*
 * Conditional queue (non-counted and counted) Functions.
 */
bool
AXP_CondQueue_Init(AXP_COND_Q_ROOT*);
bool
AXP_CondQueueCnt_Init(AXP_COND_Q_ROOT_CNT*, u32);
int
AXP_CondQueue_Insert(AXP_COND_Q_LEAF*, AXP_COND_Q_LEAF*);
bool
AXP_CondQueue_Remove(AXP_COND_Q_LEAF*, AXP_COND_Q_LEAF**);
void
AXP_CondQueue_Wait(AXP_COND_Q_HDR*);
bool
AXP_CondQueue_Empty(AXP_COND_Q_HDR*);

/*
 * ROM and Executable file reading and writing.
 */
u32
AXP_Crc32(const u8*, size_t, bool, u32);
int
AXP_LoadExecutable(char*, u8*, u32);
bool
AXP_OpenRead_SROM(char*, AXP_SROM_HANDLE*);
bool
AXP_OpenWrite_SROM(char*, AXP_SROM_HANDLE*, u64, u32);
i32
AXP_Read_SROM(AXP_SROM_HANDLE*, u32*, u32);
bool
AXP_Write_SROM(AXP_SROM_HANDLE*, u32*, u32);
bool
AXP_Close_SROM(AXP_SROM_HANDLE*);

/*
 * Buffer masking functions.  Used for not necessarily continuous buffer
 * utilization.
 */
void
AXP_MaskReset(u8*);
void
AXP_MaskSet(u8*, u64, u64, int);
void
AXP_MaskStartGet(int*);
int
AXP_MaskGet(int*, u8, int);

/*
 * ASCII/UTF-16 conversion functions.
 */
i32
AXP_Ascii2UTF_16(char*, size_t, uint16_t*, size_t*);
i32
AXP_UTF16_2Ascii(uint16_t*, size_t, char*, size_t*);

/*
 * VHD and Network to/from Native value conversions.
 */
typedef enum
{
    U8, U16, U32, U64, GUID, CRC32, CVTMax
} AXP_CVT_Types;
void
AXP_Convert_To(AXP_CVT_Types, void*, void*);
void
AXP_Convert_From(AXP_CVT_Types, void*, void*);

/*
 * File IO functions.
 */
i64
AXP_GetFileSize(FILE*);
bool
AXP_WriteAtOffset(FILE*, void*, size_t, u64);
bool
AXP_ReadFromOffset(FILE*, void*, size_t*, u64);

#endif /* _AXP_UTIL_DEFS_ */
