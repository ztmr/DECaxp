/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This source file contains The implementation of the SysAddIn and SysAddOut
 *  functionality used to communicate from CPU to/from System in the Digital
 *  Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 23-Feb-2020 Jonathan D. Belanger
 *  Initially written.
 */
#include "com-util/AXP_SysDc.h"

/*
 * AXP_SysDc_Build_Rq
 *  This function is called to build a properly formatted request that is sent
 *  from the CPU to the System over the SysAddrOut bus.
 *
 * Input Parameters:
 *  command:
 *      An enumerated value containing the command to be sent to the System by
 *      the CPU.
 *  mask:
 *      The byte, LW, or QW mask field for the corresponding I/O commands.
 *  pa:
 *      A 64-bit value representing the physical address associated with this
 *      request.
 *  id:
 *      The ID number for the MAF, VDB, or WIOB associated with the command.
 *  m1:
 *      When set, reports a miss to the system for the oldest probe.  When
 *      clear, this flag has no meaning.
 *  m2:
 *      When set, reports that the oldest probe has missed in cache.  Also,
 *      this bit is set for system-to-CPU probe commands that hit but have no
 *      data movement.  When clear, this flag has no meaning.  m1 and m2 cannot
 *      both be set.
 *  rv:
 *      When set, validates this command.  If in speculative read mode, when
 *      set validates that command.  When clear indicates a NOP.  For all
 *      non-speculative commands this flag must be set.
 *  ch:
 *      This flag is set, along with m2, when probes with no data movement hit
 *      in the Dcache or Bcache.  This response can be generated by a probe
 *      that explicitly indicates no data movement or a ReadIfDirt command that
 *      hits on a valid, but clean or shared, cache block.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
void
AXP_SysDc_Build_Rq(AXP_SysDc_Msg *msg, AXP_SysDc_Command command,
                   AXP_SysDc_Mask mask, u64 pa, u8 id, bool m1, bool m2,
                   bool rv, bool ch)
{
    msg->type = SYSDC_Request;
    msg->msgs.toSys.command = command;
    msg->msgs.toSys.mask = mask;
    msg->msgs.toSys.physicalAddress = pa;
    msg->msgs.toSys.id = id;
    msg->msgs.toSys.miss1 = m1;
    msg->msgs.toSys.miss2 = m2;
    msg->msgs.toSys.rqValidate = rv;
    msg->msgs.toSys.cacheHit = ch;
    return;
}

/*
 * AXP_SysDc_Build_ProbeRsp
 *  This function is called to build a probe response that is to be sent back
 *  to the system in response to a probe request from the CPU to the System
 *  over the SysAddrOut bus.
 *
 * Input Parameters:
 *  status:
 *      This is an enumerated value indicating the result of the probe.
 *  vdb:
 *      ID number of the VDB entry containing the requested cache block.  This
 *      field is valid when either the dm bit or vs bit is set.
 *  maf:
 *      This field contains the SharedToDirty, CleanToDirty, or
 *      STCChangeToDirty MAF entry that matched the full probe address.
 *  dm:
 *      When set, indicates that data movement should occur.
 *  vs:
 *      When set, the write victim is sent.
 *  ms:
 *      When set, the MAD address is sent.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
void
AXP_SysDc_Build_ProbeRsp(AXP_SysDc_Msg *msg, AXP_SysDc_Status status, u8 vdb,
                         u8 maf, bool dm, bool vs, bool ms)
{
    msg->type = SYSDC_ProbeRsp;
    msg->msgs.probeRsp.command = SYSDC_ProbeResponse;
    msg->msgs.probeRsp.status = status;
    msg->msgs.probeRsp.vdb = vdb;
    msg->msgs.probeRsp.maf = maf;
    msg->msgs.probeRsp.dataMovement = dm;
    msg->msgs.probeRsp.victimSent = vs;
    msg->msgs.probeRsp.mafSent = ms;
    return;
}

/*
 * AXP_SysDc_Build_Probe
 *  This function is called to build a probe request that is to be sent from
 *  the system to the CPU over the SysAddrIn bus.
 *
 * Input Parameters:
 *  dm:
 *      An enumerated value indicating what, if any, data movement is being
 *      requested.
 *  ns:
 *      An enumerated value indicating the next cache block state.
 *  sysDc:
 *      Controls data movement in to and out of the CPU.
 *  pa:
 *      The physical address associated with the probe request.
 *  id:
 *      Identifies the VDB number or the IOWB number (IOWB id is >= 8)
 *  rvb:
 *      When set clears the victim or IOWB valid bit specified in the id.
 *  rpb:
 *      When set, clears the probe valid bit specified in the id.
 *  a:
 *      When set, the CPU decrements the command outstanding counter.
 *  c:
 *      When set, the CPU decrements the uncommitted event counter used for MB
 *      acknowledge.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
void
AXP_SysDc_Build_Probe(AXP_SysDc_Msg *msg, AXP_SysDc_Probe_DM dm,
                      AXP_SysDc_Probe_Next ns, AXP_SysDc_SysDc sysDc, u64 pa,
                      u8 id, bool rvb, bool rpb, bool a, bool c)
{
    msg->type = SYSDC_Probe;
    msg->msgs.probe.dataMvmt = dm;
    msg->msgs.probe.nextState = ns;
    msg->msgs.probe.sysDc = sysDc;
    msg->msgs.probe.physicalAddress = pa;
    msg->msgs.probe.id = id;
    msg->msgs.probe.rvb = rvb;
    msg->msgs.probe.rpb = rpb;
    msg->msgs.probe.ack = a;
    msg->msgs.probe.commit = c;
    return;
}

/*
 * AXP_SysDc_Build_DataXfer
 *  This function is called to build a data transfer request that is sent from
 *  the System to the CPU over the SysAddrIn bus.
 *
 * Input Parameters:
 *  sysDc:
 *      Controls data movement in to and out of the CPU.
 *  pa:
 *      The physical address associated with the probe request.
 *  id:
 *      Identifies the VDB number or the IOWB number (IOWB id is >= 8)
 *  rvb:
 *      When set clears the victim or IOWB valid bit specified in the id.
 *  rpb:
 *      When set, clears the probe valid bit specified in the id.
 *  a:
 *      When set, the CPU decrements the command outstanding counter.
 *  c:
 *      When set, the CPU decrements the uncommitted event counter used for MB
 *      acknowledge.
 *
 * Output Parameters:
 *  None.
 *
 * Return Values:
 *  None.
 */
void
AXP_SysDc_Build_DataXfer(AXP_SysDc_Msg *msg, AXP_SysDc_SysDc sysDc, u8 id,
                         bool rvb, bool rpb, bool a, bool c)
{
    msg->type = SYSDC_DataXfer;
    msg->msgs.dataXfer.sysDc = sysDc;
    msg->msgs.dataXfer.id = id;
    msg->msgs.dataXfer.rvb = rvb;
    msg->msgs.dataXfer.rpb = rpb;
    msg->msgs.dataXfer.ack = a;
    msg->msgs.dataXfer.commit = c;
    return;
}
