#
# Copyright (C) Jonathan D. Belanger 2019-2020.
# All Rights Reserved.
#
# This software is furnished under a license and may be used and copied only
# in accordance with the terms of such license and with the inclusion of the
# above copyright notice.  This software or any other copies thereof may not
# be provided or otherwise made available to any other person.  No title to
# and ownership of the software is hereby transferred.
#
# The information in this software is subject to change without notice and
# should not be construed as a commitment by the author or co-authors.
#
# The author and any co-authors assume no responsibility for the use or
# reliability of this software.
#
# Description:
#
#   This CMake file is used to build the CommonUtilities static library.
#
# Revision History:
#   V01.000 28-Apr-2019 Jonathan D. Belanger
#   Initially written.
#
#   V01.001 22-Dec-2019 Jonathan D. Belanger
#   Changing the code layout so that includes are in the same folder as the
#   source file associated with them.
#
add_library(comutl STATIC
    AXP_Blocks.c
    AXP_Configure.c
    AXP_Dumps.c
    AXP_Exceptions.c
    AXP_Execute_Box.c
    AXP_NameValuePair_Read.c
    AXP_StateMachine.c
    AXP_SysData.c
    AXP_SysDc.c
    AXP_Trace.c
    AXP_Utility.c)

target_include_directories(comutl PRIVATE
    ${PROJECT_SOURCE_DIR}/src
    /usr/include/libxml2)
