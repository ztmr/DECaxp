/*
 * Copyright (C) Jonathan D. Belanger 2018-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains the definitions for the interface to the
 *  System Data functionality.
 *
 * Revision History:
 *
 *  V01.000 27-Jan-2018 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_SYSDATA_H_
#define _AXP_SYSDATA_H_

#include "com-util/AXP_Common_Include.h"

/*
 * Define some symbols to indicate the size of the SysData buffers.  Each
 * buffer is 64 bytes long (8 quadwords), which is the same size as a BCache
 * block.
 */
#define AXP_SYSDATA_BUFLEN      64  /* Must be equal to AXP_BCACHE_BLOCK_SIZE */
#define AXP_SYSDATA_BLOCK_LEN   (AXP_SYSDATA_BUFLEN / sizeof(u64))

/*
 * This is a single SysData buffer (8 quadwords).
 */
typedef u64 AXP_SysData_Buffer[AXP_SYSDATA_BLOCK_LEN];

/*
 * This structure contains a single buffer, plus it status.  We don't really
 * care about clean or dirty, just valid.  Also, some components may not
 * transfer a complete SysData buffer.  The length field is used to indicate
 * the number of bytes that are valid.  The id field is a unique id assigned
 * to the buffer when data is added (copied, byte for byte).
 */
typedef struct _axp_sysdata_block
{
    AXP_SysData_Buffer buffer;
    struct _axp_sysdata_block *flink;
    struct _axp_sysdata_block *blink;
    u64 id;
    u32 length;
    bool valid;
} AXP_SysData_Block;

/*
 * This structure contains between 1 and n SysData Blocks in either.  The list
 * of blocks is actually an array.  The array is maintained in id order and
 * first_free indicates the index in the array for the first free block.  If
 * first_free is equal to max_blocks, then the array is full and there is no
 * more room (which should never happen).  The list_mutex is used to make sure
 * that code that can modify the contents of this structure is single threaded.
 * The id field is used to determine what the lock order for these structures
 * may be when two need to be locked at the same time.  This is to avoid
 * deadlocks.
 */
typedef struct
{
    AXP_SysData_Block *first;
    AXP_SysData_Block *first_free;
    int list_size;
    int id;
    pthread_mutex_t list_mutex;
    pthread_cond_t list_cond;
} AXP_SysData_List;

#endif /* _AXP_SYSDATA_H_ */
