/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 22-Feb-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_SYSDC_H_
#define _AXP_SYSDC_H_

#include "com-util/AXP_Common_Include.h"
#include "com-util/AXP_Utility.h"

/*
 * The following enumeration defines all the CPU --> System commands
 */
typedef enum
{
    SYSDC_NOP,              /* b'00000' */
    SYSDC_ProbeResponse,    /* b'00001' */
    SYSDC_NZNOP,            /* b'00010' */
    SYSDC_VDBFlushRequest,  /* b'00011' */

    SYSDC_WrVictimBlk,      /* b'00100' */
    SYSDC_CleanVictimBlk,   /* b'00101' */
    SYSDC_Evict,            /* b'00110' */
    SYSDC_MB,               /* b'00111' */

    SYSDC_ReadBytes,        /* b'01000' */
    SYSDC_ReadLWs,          /* b'01001' */
    SYSDC_ReadQWs,          /* b'01010' */
    SYSDC_WrBytes,          /* b'01100' */
    SYSDC_WrLWs,            /* b'01101' */
    SYSDC_WrQWs,            /* b'01110' */

    SYSDC_ReadBlk,          /* b'10000' */
    SYSDC_ReadBlkMod,       /* b'10001' */
    SYSDC_ReadBlkI,         /* b'10010' */
    SYSDC_FetchBlk,         /* b'10011' */
    SYSDC_ReadBlkSpec,      /* b'10100' */
    SYSDC_ReadBlkModSpec,   /* b'10101' */
    SYSDC_ReadBlkSpecI,     /* b'10110' */
    SYSDC_FetchBlkSpec,     /* b'10111' */
    SYSDC_ReadBlkVic,       /* b'11000' */
    SYSDC_ReadBlkModVic,    /* b'11001' */
    SYSDC_ReadBlkVicI,      /* b'11010' */
    SYSDC_InvalToDirtyVic,  /* b'11011' */
    SYSDC_CleanToDirty,     /* b'11100' */
    SYSDC_SharedToDirty,    /* b'11101' */
    SYSDC_STCChangeToDirty, /* b'11110' */
    SYSDC_InvalToDirty      /* b'11111' */
} AXP_SysDc_Command;

/*
 * The following enumeration defines all the Probe Data movement requested.
 * This is only sent System --> CPU.
 */
typedef enum
{
    SYSDC_Probe_NOP_DM,     /* b'00' */
    SYSDC_Probe_Hit,        /* b'01' */
    SYSDC_Probe_Dirty,      /* b'10' */
    SYSDC_Probe_Any         /* b'11' */
} AXP_SysDc_Probe_DM;

/*
 * The following enumeration defines all of the type of hits acceptable for
 * this probe request.  This is only sent System --> CPU.
 */
typedef enum
{
    SYSDC_Probe_NOP_Next,   /* b'000' */
    SYSDC_Probe_Clean,      /* b'001' */
    SYSDC_Probe_CleanS,     /* b'010' */
    SYSDC_Probe_Trans3,     /* b'011' */
    SYSDC_Probe_DirtyS,     /* b'100' */
    SYSDC_Probe_Invalid,    /* b'101' */
    SYSDC_Probe_Trans1      /* b'110' */
} AXP_SysDc_Probe_Next;

/*
 * The following enumeration defines all of the I/O masks allowed and are only
 * valid on ReadBytes, ReadLWs, ReadQWs, WrBytes, WrLWs, and WrQWs commands
 * set CPU --> System.
 */
typedef enum
{
    AXP_SysDc_None = 0x00000000,
    AXP_SysDc_Byte_0 = 0x00000001,  /* b'0000.0001' */
    AXP_SysDc_Byte_1 = 0x00000010,  /* b'0000.0010' */
    AXP_SysDc_Byte_2 = 0x00000100,  /* b'0000.0100' */
    AXP_SysDc_Byte_3 = 0x00001000,  /* b'0000.1000' */
    AXP_SysDc_Byte_4 = 0x00010000,  /* b'0001.0000' */
    AXP_SysDc_Byte_5 = 0x00100000,  /* b'0010.0000' */
    AXP_SysDc_Byte_6 = 0x01000000,  /* b'0100.0000' */
    AXP_SysDc_Byte_7 = 0x10000000,  /* b'1000.0000' */
    AXP_SysDc_Word_0 = 0x00000011,  /* b'0000.0011' */
    AXP_SysDc_Word_2 = 0x00001100,  /* b'0000.1100' */
    AXP_SysDc_Word_4 = 0x00110000,  /* b'0011.0000' */
    AXP_SysDc_Word_6 = 0x11000000,  /* b'1100.0000' */
    AXP_SysDc_Long_0 = 0x01111111,  /* b'xxxx.xxx1' */
    AXP_SysDc_Long_1 = 0x01111110,  /* b'xxxx.xx10' */
    AXP_SysDc_Long_2 = 0x01111100,  /* b'xxxx.x100' */
    AXP_SysDc_Long_3 = 0x01111000,  /* b'xxxx.1000' */
    AXP_SysDc_Long_4 = 0x01110000,  /* b'xxx1.0000' */
    AXP_SysDc_Long_5 = 0x01100000,  /* b'xx10.0000' */
    AXP_SysDc_Long_6 = 0x01000000,  /* b'x100.0000' */
    AXP_SysDc_Long_7 = 0x10000001,  /* b'1000.0000' */
    AXP_SysDc_Quad_0 = 0x11111111   /* b'xxxx.xxxx' */
} AXP_SysDc_Mask;

/*
 * The following enumeration defines all of the status values returned as a
 * response to a probe request.  These are sent CPU --> System and only in a
 * ProbeResponse when the Probe request got a successful hit.
 */
typedef enum
{
    SYSDC_Hit_Clean,        /* b'00' */
    SYSDC_Hit_Shared,       /* b'01' */
    SYSDC_Hit_Dirty,        /* b'10' */
    SYSDC_Hit_Shared_Dirty  /* b'11' */
} AXP_SysDc_Status;

/*
 * The following enumeration defines all of the kinds of data movement between
 * the CPU and System.  This information is only sent System --> CPU and occurs
 * as the result of a probe request that hit, or a request for data sent or to
 * be received from the system.
 */
typedef enum
{
    SYSDC_NOP_SysDc,        /* b'00000' */
    SYSDC_ReadDataError,    /* b'00001' */
    SYSDC_ChgToDirtySucc,   /* b'00100' */
    SYSDC_ChgToDirtyFail,   /* b'00101' */
    SYSDC_MBDone,           /* b'00110' */
    SYSDC_ReleaseBuffer,    /* b'00111' */
    SYSDC_ReadData,         /* b'100xx' */
    SYSDC_ReadDataDirty,    /* b'101xx' */
    SYSDC_ReadDataShared,   /* b'110xx' */
    SYSDC_ReadDataDirtyS,   /* b'111xx' */
    SYSDC_WriteData         /* b'010xx' */
} AXP_SysDc_SysDc;

/*
 * HRM Table 4-12 EV68CB/EV68DC-to-System Command Fields:
 *
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * Cycle 1 |M1 |       Command     | PA...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * Cycle 2 | PA...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * Cycle 3 |M2 |              Mask             |CH |     ID    | PA...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * Cycle 4 |RV | PA...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 * HRM Table 4-17 EV68CB/EV68DC-to-System ProbeResponse Fields:
 *
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * Cycle 1 | 0 | 0   0   0   0   1 |Status |DM |VS |    VDB    | X...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * Cycle 2 | 0   0   0   0   0   0   0   0   0 |MS |    MAF    | X...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * Cycle 3 | 0 | X...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * Cycle 4 | X...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 * HRM Table 4-19 System-to-EV68CB/EV68DC Probe Fields:
 *
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * Cycle 1 | 1 |  Type | Next Tag  | PA...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * Cycle 2 | PA...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * Cycle 3 | 0 |     Data Mvmt     |RVB|RPB| A |      ID       | PA...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * Cycle 4 | C | PA...
 *         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 */

/*
 * The following enumeration is used to define the format of the SysDc being
 * sent or received System <--> CPU.
 */
typedef enum
{
    SYSDC_Request,  /* CPU --> System */
    SYSDC_ProbeRsp, /* CPU --> System */
    SYSDC_Probe,    /* System --> CPU */
    SYSDC_DataXfer  /* System --> CPU */
} AXP_SysDc_Type;

/*
 * The following union contains the definitions for each of the message formats
 * based on Type.
 */
typedef union
{
    struct
    {
        AXP_SysDc_Command command;
        AXP_SysDc_Mask mask;
        u64 physicalAddress;
        u8 id;
        bool miss1;
        bool miss2;
        bool rqValidate;
        bool cacheHit;
    } toSys;
    struct
    {
        AXP_SysDc_Command command;  /* b'00001' */
        AXP_SysDc_Status status;
        u8 vdb;
        u8 maf;
        bool dataMovement;
        bool victimSent;
        bool mafSent;
    } probeRsp;
    struct
    {
        AXP_SysDc_Probe_DM dataMvmt;
        AXP_SysDc_Probe_Next nextState;
        AXP_SysDc_SysDc sysDc;
        u64 physicalAddress;
        u8 id;
        bool rvb;
        bool rpb;
        bool ack;
        bool commit;
    } probe;
    struct
    {
        AXP_SysDc_SysDc sysDc;
        u8 id;
        bool rvb;
        bool rpb;
        bool ack;
        bool commit;
    } dataXfer;
} AXP_SysDc_Msgs;

/*
 * The following structure contains the SysDc message type and message union,
 * which is transferred between the CPU and System.
 */
typedef struct
{
    AXP_QUEUE_HDR header;
    AXP_SysDc_Type type;
    AXP_SysDc_Msgs msgs;
} AXP_SysDc_Msg;

#endif /* _AXP_SYSDC_H_ */
