/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This header file contains useful definitions to be used throughout the
 *  Digital Alpha AXP emulation software.
 *
 * Revision History:
 *
 *  V01.000 03-Jan-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_COMMON_INCLUDES_DEFS_
#define _AXP_COMMON_INCLUDES_DEFS_

/*
 * Includes used throughout the code.
 */
#define _FILE_OFFSET_BITS 64
#define __USE_LINUX_IOCTL_DEFS
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <signal.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <limits.h>
#include <fenv.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/ioctl.h>

/*
 * Define some standard data types.
 */
typedef unsigned char       u8;     /* 1 byte (8 bits) in length */
typedef unsigned short      u16;    /* 2 bytes (16 bits) in length */
typedef unsigned int        u32;    /* 4 bytes (32 bits) in length */
typedef unsigned long long  u64;    /* 8 bytes (64 bits) in length */
typedef __uint128_t         u128;   /* 16 bytes (128 bits) in length */
typedef char                i8;     /* 1 byte (8 bits) in length */
typedef short               i16;    /* 2 bytes (16 bits) in length */
typedef int                 i32;    /* 4 bytes (32 bits) in length */
typedef long long           i64;    /* 8 bytes (64 bits) in length */
typedef __int128_t          i128;   /* 16 bytes (128 bits) in length */

/*
 * Define some regularly utilized definitions.
 */
#define ONE_K                        1024   /* 1K */
#define TWO_K                        2048   /* 2K */
#define FOUR_K                       4096   /* 4K */
#define EIGHT_K                      8192   /* 8K */
#define THIRTYTWO_K                 32768   /* 32K */
#define SIXTYFOUR_K                 65536   /* 64K */
#define ONE_M               (u64) 1048576   /* 1M */
#define ONE_G            (u64) 1073741824   /* 1G */
#define ONE_T         (u64) 1099511627776   /* 1T */

/*
 * Test for power of 2
 */
#define IS_POWER_OF_2(value)    ((value) && !((value) & (value - 1)))

/*
 * Define some standard data type lengths (only the unique ones).
 */
#define BYTE_LEN    sizeof(u8)
#define WORD_LEN    sizeof(u16)
#define LONG_LEN    sizeof(u32)
#define QUAD_LEN    sizeof(u64)
#define CACHE_LEN   64

/*
 * Various values that are used throughout the code.
 */
#define AXP_LOW_BYTE    0x00000000000000ffll
#define AXP_LOW_WORD    0x000000000000ffffll
#define AXP_LOW_LONG    0x00000000ffffffffll
#define AXP_LOW_QUAD    (i64) 0xffffffffffffffffll
#define AXP_LOW_6BITS   0x000000000000003fll
#define AXP_LOW_3BITS   0x0000000000000007ll

#endif /* _AXP_COMMON_INCLUDES_DEFS_ */
