/*
 * Copyright (C) Jonathan D. Belanger 2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This file contains the prototype definitions for BCache interface.
 *
 * Revision History:
 *
 *  V01.000 03-Jan-2020 Jonathan D. Belanger
 *  Initially written.
 */
#ifndef _AXP_21264_BCACHE_DEFS_DEFS_
#define _AXP_21264_BCACHE_DEFS_DEFS_

#include "AXP_21264_CPU.h"
#include "com-util/AXP_System_Common.h"
#include "c-box/AXP_21264_Cbox.h"

/*
 * Prototype definitions for functions in the Cbox.
 *
 * AXP_21264_Bcache.c
 */
int
AXP_21264_Bcache_Index(AXP_21264_CPU*, u64);
u64
AXP_21264_Bcache_Tag(u64);
void
AXP_21264_Bcache_Evict(AXP_21264_CPU*, u64);
void
AXP_21264_Bcache_Flush(AXP_21264_CPU*);
u32
AXP_21264_Bcache_Status(AXP_21264_CPU*, u64);
bool
AXP_21264_Bcache_Read(AXP_21264_CPU*, u64, u8*, bool*, bool*);
void
AXP_21264_Bcache_Write(AXP_21264_CPU*, u64, u8*);
void
AXP_21264_Bcache_SetShared(AXP_21264_CPU*, u64);
void
AXP_21264_Bcache_ClearShared(AXP_21264_CPU*, u64);
void
AXP_21264_Bcache_SetDirty(AXP_21264_CPU*, u64);
void
AXP_21264_Bcache_ClearDirty(AXP_21264_CPU*, u64);

#endif /* AXP_21264_BCACHE_DEFS_DEFS_ */

