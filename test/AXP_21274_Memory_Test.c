/*
 * Copyright (C) Jonathan D. Belanger 2019-2020.
 * All Rights Reserved.
 *
 * This software is furnished under a license and may be used and copied only
 * in accordance with the terms of such license and with the inclusion of the
 * above copyright notice.  This software or any other copies thereof may not
 * be provided or otherwise made available to any other person.  No title to
 * and ownership of the software is hereby transferred.
 *
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by the author or co-authors.
 *
 * The author and any co-authors assume no responsibility for the use or
 * reliability of this software.
 *
 * Description:
 *
 *  This source file contains code to run a series of tests on memory.
 *
 * Revision History:
 *
 *  V01.000 29-Dec-2019 Jonathan D. Belanger
 *  Initially written.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/sysinfo.h>
#include "com-util/AXP_Configure.h"
#include "com-util/AXP_SysDataDefs.h"
#include "com-util/AXP_Trace.h"
#include "memory/AXP_MemoryDefs.h"

AXP_21274_SYSTEM sys;
u32 accesses;
AXP_21274_MemoryUnit generated;
u32 accessCount[AXP_COMMON_MAX_ARRAYS] = {0, 0, 0, 0};
bool pass[AXP_COMMON_MAX_ARRAYS] = {true, true, true, true};

void *arrayExerciser(void *voidPtr)
{
    AXP_SysData_Buffer buf;
    int arrayIndex = *((int *) voidPtr);
    double ratio;
    u64 offset;
    u64 arraySize, baseAddress, address;
    u64 readId, writeId;
    u32 rows, columns, myAccesses, ii, len;
    u8 split, twiceSplit;

    AXP_21274_Get_Array_Info(&sys, arrayIndex, &arraySize, &baseAddress, &rows,
                             &columns, &split, &twiceSplit);
    myAccesses = accesses / AXP_COMMON_MAX_ARRAYS;
    for (ii = 0; ii < myAccesses; ii++)
    {
        ratio = (double) rand() / (double) RAND_MAX;
        offset = (u64) ((double) (arraySize - 1) * ratio);
        address = (baseAddress + offset) & 0xFFFFFFFFFFFFFF9C;
        accessCount[arrayIndex]++;

        /*
         * Write the random data into the buffer where the memory array being
         * exercised can find it.
         */
        writeId = AXP_SysData_Add(sys.memoryOut[arrayIndex], generated,
                                  AXP_COMMON_BLOCK_SIZE);

        /*
         * This is the fourth actual interface test.  Call the the interface to
         * write the block of data into memory.
         */
        AXP_21274_Memory_Rq(&sys, WriteMem, address, &writeId);

        /*
         * Read the data we just wrote to memory.
         */
        AXP_21274_Memory_Rq(&sys, ReadMem, address, &readId);

        /*
         * Wait for the data to be put into the appropriate memoryOut buffer.
         */
        AXP_21274_Memory_Data_Ready(&sys, address);

        /*
         * Pull the data out of the memoryOut buffer.
         */
        AXP_SysData_Remove(sys.memoryIn[arrayIndex], readId, buf, &len);

        /*
         * Indicate that the memory out buffer has been read and is ready for
         * another read to be able to write into it.
         */
        AXP_21274_Memory_Buffer_Ready(&sys, address);

        /*
         * Check that the length return is what we expect it to me.
         */
        pass[arrayIndex] = len == AXP_COMMON_BLOCK_SIZE;
        if (pass[arrayIndex] == true)
        {
            pass[arrayIndex] = readId != 0;
            if (pass[arrayIndex] == true)
            {
                pass[arrayIndex] = memcmp(buf, generated,
                                          AXP_COMMON_BLOCK_SIZE) == 0;
                if (pass[arrayIndex] != true)
                {
                    printf("Failed to compare read in memory block with the"
                           " written in memory block for array #%d\n",
                           arrayIndex);
                }
            }
            else
            {
                printf("Failed to get a valid readId for the read in block"
                        " for array #%d\n", arrayIndex);
            }
        }
        else
        {
            printf("Failed to get the proper length for the read in buffer\n");
        }

        /*
         * If there was a failure, the above prints will indicate what actually
         * failed.  Here we are just going to dump out some additional
         * information about this round of testing.
         */
        if (pass[arrayIndex] == false)
        {
            printf("    in iteration: %u of %u\n", ii, myAccesses);
            printf("    at address: 0x%016llx\n", address);
            printf("    in array: %d\n", arrayIndex);
            printf("    with baseAddress: 0x%016llx\n", baseAddress);
            printf("    with maxAddress: 0x%016llx\n",
                   baseAddress + arraySize - 64);
        }
    }
    return(NULL);
}

int main()
{
    struct sysinfo sysInfo;
    pthread_t threadIds[AXP_COMMON_MAX_ARRAYS];
    AXP_21264_CONFIG config;
    AXP_21264_ARRAY_INFO arrays[AXP_COMMON_MAX_ARRAYS];
    clock_t start, end;
    u64 memSize, totalSize;
    u64 maxMemSize;
    u64 arraySize[AXP_COMMON_MAX_ARRAYS];
    u64 baseAddress[AXP_COMMON_MAX_ARRAYS];
    u32 arrayCount;
    u32 rows[AXP_COMMON_MAX_ARRAYS], banks[AXP_COMMON_MAX_ARRAYS];
    u8 split[AXP_COMMON_MAX_ARRAYS], twiceSplit[AXP_COMMON_MAX_ARRAYS];
    int arrayIndex[AXP_COMMON_MAX_ARRAYS] = {0, 1, 2, 3};
    int ii, jj;
    bool pass = true;

    /*
     * Before we get too far, get the maximum amount of memory in the host.
     * We only will allow half of the total memory to be used.  More than that
     * causes the allocate to fail.
     *
     * NOTE: We are still potentially allocating a very large chunk of memory
     *       from the system.  The size actually allocated my adversely affect
     *       overall interface performance.  It'll depend upon how much memory
     *       is actually free at the time of the test.  The OS/ needing to
     *       perform swapping will cause a performance impact.
     */
    sysinfo(&sysInfo);
    maxMemSize = sysInfo.totalram / ONE_M / 2;

    /*
     * Seed the random number generator.  Since we do not need to have the same
     * random numbers from run to run, we'll use the current time as the seed.
     */
    srand(time(0));

    /*
     * Randomly pick an amount of memory to allocate.  Note: The memory
     * requested will be rounded up to the next size, if necessary.
     */
    memSize = (u64) ((double) rand() / (double) RAND_MAX * (double) maxMemSize);
    printf("Requesting memory size = %llu MB\n", memSize);

    /*
     * Before we can do any initialization on the memory, we need to set up the
     * configuration we are going to utilize.
     */
    config.system.memory = arrays;
    config.system.arrayCount = AXP_COMMON_MAX_ARRAYS;
    totalSize = 0;
    for (ii = 0; ii < AXP_COMMON_MAX_ARRAYS; ii++)
    {
        arrays[ii].array = ii;
        arrays[ii].size = memSize / AXP_COMMON_MAX_ARRAYS;
        if (ii < (AXP_COMMON_MAX_ARRAYS - 1))
        {
            totalSize += arrays[ii].size;
        }
    }
    arrays[AXP_COMMON_MAX_ARRAYS - 1].size = memSize - totalSize;
    AXP_CopyConfig(&config);
    AXP_TraceInit();

    /*
     * Before we can do anything, we need to initialize the the memory.  This is
     * the first actual test of the interface.  We'll verify the results below,
     * if this call succeeds.
     */
    if (!AXP_21274_Memory_Init(&sys))
    {
        printf("Failed to initialize memory.\n");
        exit(0);
    }
    else
    {

        /*
         * This is the second actual test of the interface.  We are going to
         * ask it for some information that got generated as a result of the
         * previous interface call.  We should get a rounded up memory size and
         * an indicator of the number of memory arrays are in use.
         */
        AXP_21274_Get_Memory_Size(&sys, &memSize, &arrayCount);
        printf("Allocated memory size = %llu MB in %u arrays\n",
               memSize / ONE_M, arrayCount);

        /*
         * This is the third actual test of the interface.  We are going to loop
         * through each of the memory arrays returned in the previous interface
         * call and dump out its characteristics.  The size of each array should
         * add up to the memory size returned in the previous interface call.
         * The base address should start at 0 and increment by the size of the
         * previous array size.  The rows, banks, split, and twiceSplit are just
         * informational.  The base address information will be used below to
         * determine the array a particular address should be written/read.
         */
        for (jj = 0; jj < arrayCount; jj++)
        {
            AXP_21274_Get_Array_Info(&sys, jj, &arraySize[jj],
                                     &baseAddress[jj], &rows[jj], &banks[jj],
                                     &split[jj], &twiceSplit[jj]);
            printf("Array[%d]:\n", jj);
            printf("    arraySize:   %llu MB\n", arraySize[jj] / ONE_M);
            printf("    baseAddress: 0x%016llx\n", baseAddress[jj]);
            printf("    rows:        %d\n",
                   rows[jj] == 0 ? 11 : rows[jj] == 1 ? 12 : 13);
            printf("    banks:       %d\n", (int) banks[jj] + 1);
            printf("    split:       %s\n", split[jj] ? "true" : "false");
            printf("    twiceSplit:  %s\n", twiceSplit[jj] ? "true" : "false");
            sys.memoryIn[jj] = AXP_SysData_Create(1);
            sys.memoryOut[jj] = AXP_SysData_Create(1);
        }
        printf("\n");
    }

    /*
     * Randomly pick the number of times we are going to iterate on writing to
     * memory and then reading what was written back out and comparing the
     * results.
     */
    accesses = (u32) ((float) rand() / (float) RAND_MAX * 10000000.0);
    printf("Performing %d,%03d,%03d memory writes and reads\n",
           (accesses / 1000000) % 1000, (accesses / 1000) % 1000,
           accesses % 1000);

    /*
     * Get the start time for the tests.  The granularity is not necessarily the
     * most accurate, but it is sufficient for these tests.
     */
    start = clock();

    /*
     * OK, here is the meat of the tests, create four threads, one per array,
     * and get them started.
     */
    for (ii = 0; ii < AXP_COMMON_MAX_ARRAYS; ii++)
    {
        pthread_create(&threadIds[ii], NULL, arrayExerciser,
                       (void *) &arrayIndex[ii]);
    }

    /*
     * Next, we wait for them to complete.
     */
    for (ii = 0; ii < AXP_COMMON_MAX_ARRAYS; ii++)
    {
        pthread_join(threadIds[ii], NULL);
    }

    /*
     * OK, the above iterations have completed, either successfully or not.
     * Either way, get the end time.  If all tests passed, then let's dump out
     * some performance numbers.
     */
    end = clock();
    if (pass == true)
    {
        long double seconds = (end - start) / (long double) CLOCKS_PER_SEC;
        double accessesPerSecond = (double) accesses / seconds;
        u64 accPerSec = (u64) accessesPerSecond;
        u64 opsPerSec = accPerSec * 2;
        u64 bytesMoved = AXP_COMMON_BLOCK_SIZE * accesses * 2;
        u32 aps1000 = accPerSec % 1000;
        u32 apsMil = (accPerSec / 1000) % 1000;
        u32 apsBil = (accPerSec / 1000000) % 1000;
        u32 ops1000 = opsPerSec % 1000;
        u32 opsMil = (opsPerSec / 1000) % 1000;
        u32 opsBil = (opsPerSec / 1000000) % 1000;
        u64 mBytesMoved = bytesMoved / ONE_M;
        double bytesPerSecond = (double) bytesMoved / seconds;
        u64 mBytesPerSecond = (u64) (bytesPerSecond / (double) ONE_M);

        printf("\nall tests passed in %2.2Lf seconds\n", seconds);
        printf("    64 byte writes and reads per second = %u,%03u,%03u\n",
               apsBil, apsMil, aps1000);
        printf("    64 byte memory operations per second = %u,%03u,%03u\n",
               opsBil, opsMil, ops1000);
        printf("    total MB moved in and out of memory = %llu\n", mBytesMoved);
        printf("    transfer rate (MB/second) = %llu\n", mBytesPerSecond);
        printf("    Array accesses:\n");
        printf("        [0]: %8d    [1]: %8d\n",
               accessCount[0], accessCount[1]);
        printf("        [2]: %8d    [3]: %8d\n",
               accessCount[3], accessCount[3]);
    }

    /*
     * If all tests passed, then return a 0 to the caller, otherwise a -1.
     */
    return (pass == true ? 0 : -1);
}
